---
tags: eBike
partial: e-bike-kid-prices
---

### Child seat and child helmet

- 1 day: 5 €
- 3 days: 12 €
- 5 days: 20 €
