---
tags: eBike
partial: e-bike-season-prices
---

### par vélo incl. helmet

- Basse saison
  - (April - Juin)
  - 1 jour: 25 €
  - 3 jours: 70 €
  - 5 jours: 105 €
- Haute saison
  - (Juillet – October)
  - 1 jour: 30 €
  - 3 jours: 80 €
  - 5 jours: 125 €
