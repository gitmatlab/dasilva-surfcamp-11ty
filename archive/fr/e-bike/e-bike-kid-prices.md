---
tags: eBike
partial: e-bike-kid-prices
---

### Siège enfant et casque enfant

- 1 jour: 5 €
- 3 jours: 12 €
- 5 jours: 20 €
