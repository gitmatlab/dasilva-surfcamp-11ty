---
tags: eBike
partial: e-bike-season-prices
---

### pro E-Bike inkl. Fahrradhelm

- Low Season
  - (April - Juni)
  - 1 Tag: 25 €
  - 3 Tage: 70 €
  - 5 Tage: 105 €
- High Season
  - (Juli – Oktober)
  - 1 Tag: 30 €
  - 3 Tage: 80 €
  - 5 Tage: 125 €
