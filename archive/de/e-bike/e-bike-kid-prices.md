---
tags: eBike
partial: e-bike-kid-prices
---

### Kindersitz inkl. Kinderhelm

- 1 Tag: 5 €
- 3 Tage: 12 €
- 5 Tage: 20 €
