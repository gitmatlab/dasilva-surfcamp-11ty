# CMS for Da Silva Surfcamp

- [CMS for Da Silva Surfcamp](#cms-for-da-silva-surfcamp)
  - [Project Todos](#project-todos)
    - [Im Trello](#im-trello)
    - [Backlog](#backlog)
  - [Internationalisation](#internationalisation)
    - [fr](#fr)
  - [Research](#research)
    - [Tools](#tools)
    - [Meta description](#meta-description)
    - [Meta Title](#meta-title)
  - [CSS Tricks](#css-tricks)
  - [Image Sizes Dynamically](#image-sizes-dynamically)
    - [Responsive Images Generator](#responsive-images-generator)
    - [11ty Image Generator](#11ty-image-generator)
  - [XHR, Fetch and CORS](#xhr-fetch-and-cors)
  - [Maps](#maps)
  - [Instagram Integration](#instagram-integration)
  - [CMS Requirements](#cms-requirements)
  - [Site Generator Requirements](#site-generator-requirements)
  - [Web Scraper and Crawler](#web-scraper-and-crawler)
  - [Multilingual with 11ty](#multilingual-with-11ty)
  - [Lightbox](#lightbox)
  - [Dropdown Menu](#dropdown-menu)
  - [Masonry](#masonry)
  - [Ghost CMS](#ghost-cms)
  - [Booking Layer](#booking-layer)
  - [CDN](#cdn)
    - [cloudflare](#cloudflare)
    - [unpack](#unpack)
    - [imageresizerio](#imageresizerio)
  - [CSS](#css)
  - [Lazy Loading](#lazy-loading)
    - [Iframe](#iframe)
    - [Youtube](#youtube)
    - [Preloading fonts](#preloading-fonts)
- [Loadtest](#loadtest)
  - [Speed results](#speed-results)
  - [Testing](#testing)
    - [Speedtest tool](#speedtest-tool)
- [Project Archive](#project-archive)
  - [Anforderungen (dez 2020)](#anforderungen-dez-2020)
  - [Open](#open)
- [Credits](#credits)


## Installation


```sh
# modify your env
vi .env
# install and start
npm i
npm start
```

## Project Todos

- [Trello](https://trello.com/b/Rv9TiS3e/surfcamp-website)

## Research

### Tools

- [Google Search Preview](https://seositecheckup.com/seo-audit/google-search-results-preview-test/)
  - https://seositecheckup.com/seo-audit/google-search-results-preview-test/dasilva.alabaster.de
- [SEO Online Checker](https://freetools.seobility.net/de/seocheck/dasilva.alabaster.de)
- [Meta Imaage Maker](https://www.kapwing.com/studio/editor)
-  [fav icon maker](https://www.favicon-generator.org)

## Deploy

- There are 2 sites:
  - development and testing site
  - production site

### Development site

- The site is available under: https://dev.dasilve-surfcamp.de Mind the *dev.* in front on the URL.
- This site is for testing. Here you can see all the changes. You can check, if it looks as expected and if everything is OK and nothing broken.
- Site is updated automatically by commiting changes in git. Every commit triggers is building the site. When the build is done (see the gitlap pipeline), the changes are visible.

### Production Site

- The site is available under: dasilve-surfcamp.de (without the dev.)
- This site is for the customers. It's the "production" site.
- Once the dev is tested, you create a tag
- Mind, that the tag has a strict convention. For example: If the current tag is 1.5.12 the next tag has to be 1 number higher: 1.5.13. Only change the very last number.

### Rollback on Production

- In case the production site is broken, you can go back in history.
- Create a new tag. Remember always count up the last number. 1.5.13 becomes 1.5.14.
- Write in the tag comment the tag number you want to have. Pretend we have want to go back to version 1.5.12. Write as tag comment: ROLLBACK: 1.5.12
- Make sure the word rollback ist in capital letters: `ROLLBACK`, and is followed by a collom and one space `: ` and then then number `1.5.12`
- Warning: you can go back only 4 tags, because old versions are deleted

## Seo

### Meta description
  - [good meta description examples](https://www.seobility.net/de/wiki/Meta-Description#Positive_Effekte_einer_guten_Meta-Description)
  - [meta description instruction](https://www.seobility.net/de/blog/die-perfekte-seo-meta-description/)
  - 1000pixel ~ 156 characters
  - must be human readalbe
  - no double words
  - must include main key word (e.g. surfing)
  - `Urlaub in Portugal in einem typischen portugiesischen Landhaus in Standnähe | Surfschool | Mountainbike | Pferde reiten | Kinder willkommen`

### Meta Title
  - 580px, 70 characters
  - No keyword stuffing but 3 relevant key words. 1st key word most important
  - Build a sentence, be crisp
  - Don't use a dash (-) but rather a pipe (|)
  - `Da Silva Camp Portugal » Surfing und Mountainbike mit der Familie`


## CSS Tricks

- [box shadow examples](https://getcssscan.com/css-box-shadow-examples)
- [hover effects](https://thebrandsmen.com/css-image-hover-effects/)

## Image Sizes Dynamically

To serve different versions scaled for different devices, you need to use the HTML srcset attribute in your img tags, to specify more than one image size to choose from.

```html
<img srcset="large-img.jpg 1024w,
middle-img.jpg 640w,
small-img.jpg  320w"
src="small.jpg"
/>
```

```js
const configs = [
    {width: '20%', rename: {suffix: '@1x'}},
    {width: '40%', rename: {suffix: '@2x'}},
    {width: '60%', rename: {suffix: '@3x'}},
    {width: '80%', rename: {suffix: '@4x'}},
    {width: '100%', rename: {suffix: '@5x'}}
]
const images = [
  path.join(__dirname, 'aileen.jpg'),
  path.join(__dirname, 'kevin.jpg')
]

generateResponsiveImages(images, configs)
```

### Responsive Images Generator

https://github.com/felixrieseberg/responsive-images-generator

### 11ty Image Generator

https://www.11ty.dev/docs/plugins/image/


## XHR, Fetch and CORS

Mozilla Resources:
- [XHR](https://developer.mozilla.org/en-US/docs/Learn/Forms/Sending_forms_through_JavaScript)
- [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)
- [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)
- [Using Fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
- youtube showcasing fetch api: https://www.youtube.com/watch?v=FN_ffvw_ksE

## Maps

- [Mapbox](https://github.com/mapbox/mapbox-gl-directions/blob/master/API.md) works with [Open Street Map](https://www.openstreetmap.org/#map=7/39.602/-7.839)
- [Mapbox Directions](https://docs.mapbox.com/mapbox-gl-js/example/mapbox-gl-directions/)

## Instagram Integration

- https://www.instagram.com/dasilvasurfcamp/
- #dasilvasurfcamp
- Benutzerkonto:
dasilvasurfcamp (17841401066982260)

- dev instructions: https://developers.facebook.com/docs/instagram/oembed
- javascript lib: https://github.com/stevenschobert/instafeed.js
- node module: https://www.npmjs.com/package/node-instagram

- To get a Client Access Token, sign into your App Dashboard and navigate to Settings > Advanced > Security > Client Token.


GET http://graph.facebook.com/17841401066982260/media


## Multilingual with 11ty

- [Languag Switcher Multilangual Jamstack ](https://www.webstoemp.com/blog/language-switcher-multilingual-jamstack-sites/)

## Lightbox

- [w3school](https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_js_lightbox)

## Dropdown Menu

- https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_js_dropdown_hover


## Masonry

- Colocate Desandro
  - https://github.com/desandro/colcade
  - https://codepen.io/gitmathub/pen/LYZaMKK
- Flexamasonry
  - https://github.com/gilbitron/flexmasonry
  - pure css
- Bricks
  - https://github.com/callmecavs/bricks.js
  - Fixed width
  - fast
- Justified Gallery
  - https://github.com/miromannino/Justified-Gallery
  - fixed height

- Hover https://www.w3schools.com/howto/howto_css_image_overlay.asp

## Ghost CMS

- Docs: https://ghost.org/docs/
- Ghost + 11ty: https://ghost.org/changelog/eleventy/
- Ghost 11ty Starter: https://github.com/TryGhost/eleventy-starter-ghost/
- Export from Wordpress to Ghost: https://ghost.org/tutorials/wordpress-to-ghost/

- Export from Ghost to 11ty: https://github.com/reverentgeek/ghost-to-eleventy-exporter


## Booking Layer

https://www.bookinglayer.com

- direct link to bed and breakfast for a couple in English, from the March 1st:
 - https://da-silva-bike-surf-camp.bookinglayer.io/frontoffice/#/product/59455?lang=en&start=2021-03-01&duration=10&couple=1 


## CDN

https://hostingcrown.com/best-image-cdn

### cloudflare

https://www.cloudflare.com/

Check which name server is respondig

```shell
dig -t NS dasilva.alabaster.de
```

### unpack

- https://unpkg.com
- for npm packages
- hosted on cloudflare + google cloud
- example: `<script src="https://unpkg.com/colcade@0/colcade.js"></script>`


### imageresizerio

- https://imageresizer.io/demo
- user: mat@alabaster.de
- -2020-1

- admin interface:
- https://dashboard.imageresizer.io/media


## CSS

- Seamless Responsive Photo Grid
 - https://css-tricks.com/seamless-responsive-photo-grid/

## Lazy Loading

### Iframe

- https://codepen.io/m3g4p0p/pen/oepWVR

```html
  <iframe src="video-player.html" title="..." loading="lazy"></iframe>
```
- iframe tips
  - https://web.dev/iframe-lazy-loading/

### Youtube 

- lite-youtube-embed claims to load 200x faster
  - https://github.com/paulirish/lite-youtube-embed

### Preloading fonts

```html
<link rel="preload" as="font" href="{% assetsUrl %}/fonts/tuitypelight-regular.woff2" type="font/woff2" crossorigin="anonymous">
```


## Speed results

*Ohne Cloudflare*
![](docs/images/load-01-surfcamp.jpg)
*Cloudflare*
![](docs/images/load-cloudflare.jpg)
![](docs/images/load-02-alabaster.jpg)
![](docs/images/load-03-babylon.jpg)



## Testing

test image loading speed
```bash
# dasilve surfcamp
curl -o /dev/null -s -w %{time_total}\\n  https://www.dasilva-surfcamp.de/wp-content/uploads/2016/05/portugal_surfcamp-e1586447532869-1200x900.jpg

# alabaster
curl -o /dev/null -s -w %{time_total}\\n  https://alabaster.de/load-test/images/portugal_surfcamp-e1586447532869-1200x900.jpg
```

resize to 228 height
```
curl -o /dev/null -s -w %{time_total}\\n  https://im.ages.io/4FtAEiH1l1\?height\=228
```

### Speedtest tool

- https://www.dotcom-tools.com/website-speed-test.aspx

## Credits

- country icons
  - https://www.svgrepo.com/vectors/portuguese/
  - openstreetmap.org
