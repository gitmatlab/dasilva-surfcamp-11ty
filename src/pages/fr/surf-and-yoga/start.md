---
tags: yoga
partial: start
---

## Surfez mieux et sentez-vous bien grâce au yoga

Trouvez avec nous la décélération, la concentration et la paix dont vous avez besoin pour arriver complètement à vous-même. Nos sessions de yoga vous offrent l'équilibre parfait pour le surf et renforcent votre corps pour une session de surf réussie.

Le yoga et le surf sont faits l'un pour l'autre - tandis que vous êtes pleinement en action en surfant, vous trouvez la paix intérieure dans le yoga.

Nous offrons un mélange de yoga classique Hatha et de Vinyasa moderne adapté à tous les niveaux. Que vous soyez débutant ou yogi expérimenté, vous trouverez les bonnes sessions avec nous.

Nous voulons contribuer à promouvoir votre santé et à harmoniser votre esprit, votre corps et la nature.
