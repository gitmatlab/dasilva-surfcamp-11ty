---
tags: yoga
partial: including
---

<div class="h3">inclusif</div>

* 7 nuits en Bed & Breakfast au Da Silva Surfcamp<br/>(comme décrit ci-dessous)
* 10 cours de surf intensif de 2 heures<br/>(2 heures par session / 2 sessions par jour)
* équipement de surf complet pour le cours<br/>(planche de surf, leash, combinaison)
* Navette pour les plages avec les meilleures vagues de notre région
* 5 x 1h de yoga après le cours de surf

## chambres d'hôtes

* Hébergement
* petit-déjeuner buffet quotidien
* Navette quotidienne vers la ville balnéaire la plus proche
* location gratuite de beach cruiser
* WiFi gratuit dans toute la maison
* à partir d'une semaine de séjour : 2x barbecue par semaine (également végétarien)
