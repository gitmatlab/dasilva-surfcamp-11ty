---
permalink: /fr/environs/index.html
layout: surroundings.njk
tags: surroundings
locale: fr
navigation: Environs

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Environs, loisirs et activités"

# max 158 characters
metaDescription: "Grandes plages, paysages et villes | Amusement dans le Dinopark, piscine, salle d'évasion, karting ou tennis et bien plus encore"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surroundings.jpg"
---
