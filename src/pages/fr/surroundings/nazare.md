---
tags: surroundings
partial: nazare
mapLink: https://goo.gl/maps/CqsQGckf1wFVMJVJA
---

Les vagues monstres de renommée mondiale ne peuvent être vues qu'ici (60 km au nord) hors saison, mais l'endroit est par ailleurs très beau et vaut absolument le détour.
