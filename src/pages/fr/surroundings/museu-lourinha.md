---
tags: surroundings
partial: museu-lourinha
webLink: https://museulourinha.org
mapLink: https://bit.ly/2D59BjA
---

### Museu da Lourinhã

Envie de voyager dans le temps ? Alors laissez-nous vous raconter l'histoire passionnante de la région autour de Lourinhã. Y compris le squelette de dinosaure, la vie et le travail des gens et un savoir-faire fascinant de différentes époques.
