---
tags: surroundings
partial: minigolf
mapLink: https://bit.ly/2Zrgjdb
---

### Golf miniature Animar Praia da Areia Branca

Vous n'avez pas eu de bal tranquille depuis longtemps ? Pas une chose - dirigez-vous simplement vers la plage jusqu'à Praia da Areia Branca et coulez les billes sans serrer de la hanche. Très amusant avec les enfants ou pour les grands avec une boisson fraîche comme niveau de difficulté augmenté 😉
