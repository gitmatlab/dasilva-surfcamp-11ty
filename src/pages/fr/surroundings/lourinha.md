---
tags: surroundings
partial: lourinha
mapLink: https://goo.gl/maps/pp7DREaLrA5FBZfHA
---

Le chef-lieu (5 km) où vous trouverez des banques, des pharmacies, des médecins, la poste, de grands supermarchés, des boutiques, un marché hebdomadaire et des cafés typiques et de bons restaurants.
