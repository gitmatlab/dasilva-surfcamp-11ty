---
tags: surroundings
partial: praia-areia-branca
mapLink: https://goo.gl/maps/SvoMUacFBzMmB7bj7
---

Notre "home spot" (2 km, 10 minutes en vélo) avec une promenade sur la plage, de bonnes vagues et une vie nocturne animée. Le bus express de Lisbonne s'arrête également ici.
