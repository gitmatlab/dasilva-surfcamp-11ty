---
tags: surroundings
partial: gokart
webLink: www.dinokart.com.pt
---

### Go Kart

Juste faire un tour ? Va dans un Dinokart - et bien sûr pas un seul. Accélérez au lieu de courir ! Sensation de cabriolet au format mini - vous ne pouvez pas vous rapprocher de l'asphalte !
