---
tags: surroundings
partial: start
---

# Environs

Autour de notre camp de surf, il y a principalement beaucoup de nature rustique. Vous pourrez faire de merveilleuses promenades le long des chemins de campagne. De l'autre côté de la vallée se trouve le village agricole typique "Zambujeira". Il n'y a rien à part un petit café et une mini épicerie. Une visite vaut tout de même la peine, car c'est vraiment très original là-bas. Une fois par an, au mois d'août, il y a une grande fête de village, pour laquelle même les arrière-grands-mères se déguisent. Sinon, si vous voulez sortir le soir, vous devez vous rendre à Praia da Areia Branca ou à Peniche, mais nous vous y emmènerons avec notre navette. Vous n'avez donc pas à vous inquiéter, vous ne vous ennuierez certainement pas.
