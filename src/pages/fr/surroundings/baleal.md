---
tags: surroundings
partial: baleal
mapLink: https://goo.gl/maps/n6XtzJkkn3iUbPgF8
---

La Mecque du surf en Europe (12 km au nord) avec plus de 40 camps de surf et une vie nocturne animée. Un peu encombré, mais ça vaut toujours le détour.