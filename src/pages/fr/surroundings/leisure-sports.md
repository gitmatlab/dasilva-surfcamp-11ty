---
tags: surroundings
partial: leisure-sports
---

## Temps libre

Nous vous offrons toujours la possibilité de faire quelque chose avec nous après avoir surfé. Il existe des offres presque illimitées pour cela. Vous pouvez trouver ici quelques exemples du passé tels que des événements de longboard, des soirées de fado ou des sessions de karting.
Il n'y a pas de programme fixe, mais nous discutons sur place de ce que vous avez le plus envie de faire. Ensuite, nous procédons à la majorité. Nous nous occupons du trajet avec notre navette. Tous les frais encourus sur les lieux d'excursion sont à la charge de tous. Tout est volontaire et s'arrange spontanément et individuellement sur place.

## Des sports

Skateboard, wakeboard, kite surf, karting, tennis, golf, plongée, équitation, parapente et bien plus et plus récemment [VTT]({{links[locale].mountainBike.path}}).

Si les vagues sont trop petites ou trop grosses, s'il y a trop de tempête ou s'il pleut des chats et des chiens, vous ne pouvez pas surfer. Les cours de surf ne sont annulés qu'en cas de force majeure. Dans un tel cas, nous vous offrons les activités sportives ci-dessus à titre de compensation, mais malheureusement nous ne pouvons pas couvrir les frais.
