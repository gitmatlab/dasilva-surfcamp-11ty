---
permalink: /fr/infos-vacances/index.html
layout: plain-text.njk
tags: info
locale: fr
navigation: Info

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Info vacances"

# max 158 characters
metaDescription: "Informations de bien-être importantes sur la restauration, les moustiques, le lavage des vêtements et bien plus encore."
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Info vacances

<div class="h2">Bienvenue dans la
<br/>Da Silva Surfcamp</div>
<div class="h2">Ola e bem vindo !</div>

Nous sommes impatients de vous accueillir en tant qu'invités. Des journées absolument détendues pleines de soleil et de vagues grandioses vous attendent.

L'équipe du camp est toujours là pour vous - nous sommes prêts à répondre à vos questions, à faire des suggestions ou à vous aider là où nous le pouvons. Nous sommes là pour faire de vos vacances avec vous la façon dont vous les imaginez !

Vous trouverez dans les pages suivantes un petit récapitulatif des points clés à connaître afin de rendre les vacances aussi agréables pour tous sur place.

**Nous vous souhaitons un agréable moment parmi nous**

### Satisfaction

Notre plus grand souhait est que vous soyez satisfait de tout chez nous et que vous gardiez un bon souvenir de votre séjour. Parlez-nous simplement si vous n'êtes pas satisfait de l'un ou de l'autre, nous essaierons de changer cela dès que possible.

### Petit-déjeuner

Un petit déjeuner frais et copieux vous attend chaque matin dans la cuisine du bâtiment principal. Les horaires du petit-déjeuner dépendent de la prise en charge de l'école de surf. En général – petit-déjeuner une heure avant le départ. Même chose pour tout le monde. (Si le ramassage est très tôt, le petit-déjeuner sera bien sûr laissé à tous ceux qui sont encore là). Quand tout le monde aura fini, nous débarrassons le petit-déjeuner, mais au plus tard à 10 heures. Cependant, nous vous demandons votre aide pour la propreté générale de la cuisine - votre vaisselle et vos couverts passent au lave-vaisselle, les casseroles et poêles usagées sont lavées indépendamment après le repas.

### Réfrigérateurs

Pendant votre séjour, un délicieux petit-déjeuner est servi tous les jours et deux soirs par semaine (toujours le dimanche et le vendredi), nous organisons ensemble un grand barbecue. Nous vous demandons de ne pas utiliser le buffet du petit déjeuner pour lubrifier un panier repas pour le reste de la journée, donc on ne calcule pas et tout le monde devrait être rassasié dès le petit déjeuner ;)

Les réfrigérateurs invités dans le bâtiment principal et les compartiments sur la grande étagère sont disponibles pour votre restauration ultérieure. Ceux-ci sont fournis avec les noms des salles. Si vous êtes sur place sans voiture - pas de problème, nous serons heureux de vous conduire au supermarché de Lourinhã, parlez-nous simplement. De plus, à Praia da Areia Branca, vous trouverez un petit supermarché, un magasin de fruits et même un petit marché à droite. Si vous voulez sortir manger ou commander quelque chose, parlez-nous simplement - en plus des recommandations, nous pouvons également faire des voyages avec vous.

### Mouches et moustiques

Malheureusement, nous avons beaucoup de mouches. Il est donc absolument important que toutes les portes et fenêtres qui ne sont pas équipées de moustiquaires restent fermées à tout moment.

Il en va de même pour les moustiques. Surtout au crépuscule, toutes les fenêtres et portes doivent être absolument fermées. Malheureusement, certains moustiques parviennent toujours à pénétrer dans les chambres et causent des nuits blanches à nos hôtes.
périodes de repos

Le surf, c'est nul - certains d'entre vous le savent déjà, d'autres le remarqueront rapidement. Les rondes confortables ne doivent pas être interrompues, mais veuillez noter que nous appelons au repos au lit à partir de 23h00. Veuillez faire attention si les gens veulent dormir plus tôt.

### Les serviettes

Chaque invité reçoit une grande et une petite serviette à son arrivée. Ceux-ci sont automatiquement échangés par nous les jeudis et dimanches. Si, pour une raison quelconque, vous avez besoin d'une serviette fraîche entre les deux, veuillez nous en informer. S'il vous plaît ne prenez pas ces serviettes à la plage! Vous devriez avoir vos propres serviettes de bain pour cela.

### Linge de maison

Les lits sont fraîchement faits à l'arrivée. Si vous êtes avec nous pendant plus d'une semaine, vous pouvez vous procurer des draps propres. Veuillez nous en parler.
clé

Chaque chambre dispose d'une clé qui se trouve dans la serrure à votre arrivée. Les Tiny Houses ont une boîte à clés à l'avant. Si possible, laissez votre chambre ouverte les jeudis et dimanches afin que nous puissions disposer des serviettes propres et passer l'aspirateur une fois.

### Machine à laver

Si vous souhaitez faire votre lessive, vous pouvez utiliser la machine à laver dans notre buanderie. Nous facturons 5 EUR par machine pour cela, y compris le détergent. Si cela ne vaut pas la peine pour une personne, vous pouvez bien sûr aussi les combiner pour remplir une machine à laver.

### Séchez vos vêtements

Sur le site, vous trouverez plusieurs endroits où vous pourrez suspendre vos serviettes de plage et vos combinaisons pour les faire sécher. D'une part vous trouverez des cordes à linge derrière l'aire de jeux et d'autre part sur le mur à côté de la cuisine.

### Ordre et Propreté

En général, assurez-vous de toujours tout laisser tel que vous l'avez trouvé. Nous vous demandons de laver vous-même la vaisselle, les couverts, les casseroles et les poêles usagés ou d'utiliser le lave-vaisselle. Il en va de même pour les bouteilles vides, les verres - ceux-ci peuvent être rangés le soir même -
