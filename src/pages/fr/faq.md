---
permalink: /fr/faq/index.html
layout: plain-text.njk
tags: faq
locale: fr
navigation: FAQ

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "FAQ"

# max 158 characters
metaDescription: "Questions sur l'école de surf, la réservation, l'hébergement et les repas et bien plus encore."
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---


# FAQ

#### Foire aux questions et leurs réponses


### Dois-je suivre le cours débutant ou avancé ?

Les débutants et les apprenants avancés sont réunis dans un seul cours. Nous avons constaté que cela fonctionne très bien. Bien sûr, nos moniteurs de surf se concentrent sur les compétences de chacun, afin que chacun en profite. Au-dessus d'un certain niveau (avancé), un moniteur de surf ne peut rien vous apprendre de toute façon. La seule chose qui aide est la pratique, la pratique, la pratique...

### Quelle est votre moyenne d'âge ?

L'âge moyen chez nous est d'environ 20-25 ans, mais il y a aussi des personnes plus jeunes et parfois beaucoup plus âgées qui apprennent à surfer avec nous. Parfois, des familles entières viennent même, de sorte que les parents et leurs enfants suivent un cours ensemble.

### Où est l'aéroport le plus proche ? Comment se passe l'arrivée ?

L'aéroport le plus proche est à Lisbonne, à environ 70 km de notre camp. De là, il y a un "Aeroshuttle" jusqu'à la gare routière, qui coûte 3,50 EUR par personne. Le trajet en bus express de Lisbonne à Praia da Areia Branca coûte 8 EUR par personne. Vous pouvez également prendre un taxi jusqu'à la gare routière. Nous vous proposons également un transfert aéroport (prix pour une personne 70 EUR, chaque personne supplémentaire +10 EUR).

### Comment se passe la restauration ?

Au Da Silva Surfcamp, il y a un délicieux petit-déjeuner buffet tous les matins et un dîner commun (barbecue) deux fois par semaine. Les autres soirs, vous pouvez cuisiner vous-même dans la cuisine de notre surf house. La plupart du temps, les habitants de la surf house cuisinent ensemble et partagent les frais. Ou parfois l'un cuisine et parfois l'autre. Tout cela est discuté de manière flexible sur place. Il y a aussi des restaurants très bon marché à Praia da Areia Branca, vous n'avez donc pas à cuisiner dans la surf house tous les soirs.

### Quel jour de la semaine commencent les cours de surf (début des cours) ?

Les cours de surf commencent toujours le lundi et si les conditions de vagues et de météo le permettent, les cours ont lieu tous les jours, de sorte que le dernier cours a lieu le vendredi. Cependant, il arrive souvent que l'un ou l'autre cours doive être annulé en raison des conditions mentionnées et n'ait alors lieu que le week-end. Dans l'ensemble, l'ensemble est très flexible et est souvent modifié individuellement sur place et après consultation des moniteurs de surf.

### Dans quelle langue se déroulent les cours de surf ?

Nos moniteurs de surf ne parlent que portugais et anglais. Mais ce n'est pas du tout un problème. Il s'agit principalement de leçons pratiques, c'est-à-dire que la plupart des choses sont simplement démontrées ou expliquées avec les mains et les pieds. En tout cas, il n'y a jamais eu de difficultés de communication entre enseignants et élèves.