---
permalink: /fr/skateboarding/index.html
layout: skateboarding.njk
tags: skateboarding
locale: fr
navigation: Skateboarding

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Skateboarding"

# max 158 characters
metaDescription: "Propre mini rampe sur le camp | Skate Park Lourinha | Surf & Skateboard"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/skateboarding.jpg"
---
