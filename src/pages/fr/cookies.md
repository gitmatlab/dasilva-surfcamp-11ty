---
permalink: /fr/cookies/index.html
layout: plain-text.njk
tags: cookies
locale: fr
navigation: Cookies

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Cookie"

# max 158 characters
metaDescription: "Cookie | Privacy Policies"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Cookies

Nous utilisons 2 types de cookies : les cookies marketing et les cookies fonctionnels.

Vous pouvez découvrir comment autoriser ou refuser les cookies sur cette page.

## Cookies marketing

Les cookies marketing nous aident à comprendre comment vous nous avez trouvés. Cela nous permet de mieux coordonner nos campagnes marketing. Les cookies installés sont évalués par Google Analytics. Vous pouvez trouver des détails sur le sujet de la protection des données sur notre [{{ links[locale].privacyPolicies.title }}]({{ links[locale].privacyPolicies.path }}).

Si vous ne voulez pas de cookies marketing, vous pouvez cliquer sur "Rejeter les cookies marketing" sur la **bannière de consentement aux cookies** en bas de la page. Vous nous rendez service en cliquant sur "Accepter les cookies" :-)

## Cookies fonctionnels

Les cookies fonctionnels sont des cookies nécessaires pour des raisons techniques. Nous avons 2 cookies sur notre site : un pour les vidéos et un pour la réservation en ligne.

### cookies vidéo

Le cookie vidéo est défini exactement lorsque vous cliquez sur une vidéo. Nous utilisons le lecteur vidéo de "Vimeo", qui a besoin du cookie pour savoir quand le lecteur vidéo est chargé et prêt à lire la vidéo. Vous pouvez trouver plus de détails ici : <a href="https://vimeo.com/cookie_policy" target="_blank">Politique relative aux cookies de Vimeo</a>

Si vous ne voulez pas de cookie vidéo, cliquez sur aucune vidéo.

### Cookie de réservation en ligne

Lorsque vous accédez à la réservation en ligne, un cookie est utilisé. Nous utilisons le prestataire de services "Bookinglayer" pour gérer la réservation et le paiement. Ce fournisseur de services ne peut être utilisé qu'avec des cookies. Vous pouvez trouver plus de détails ici : <a href="https://www.bookinglayer.com/cookie-policy" target="_blank">Politique relative aux cookies de la couche de réservation</a>.

Si vous ne souhaitez pas ce cookie de réservation, vous pouvez utiliser notre [{{ links[locale].bookingRequest.title }}]({{ links[locale].bookingRequest.path }}) ou nous contacter directement [{{ links[locale].contact.title }}]({{ links[locale].contact.path }}).
