---
tags: surfLodges
partial: quarto-por-do-sol
---

La cour de la Casa Francisco et du Studio Prata propose le Quarto Por do Sol pour deux personnes comme troisième logement. Le nom signifie "Sunset Room" car le soleil brille à travers la fenêtre à partir de midi et vous pouvez voir de fantastiques couchers de soleil le soir. Les clients disposent d'une chambre avec un lit double, d'une petite kitchenette, d'une table à manger, d'une télévision connectée et d'une salle de douche. Des meubles de jardin et un espace barbecue sont disponibles dans la cour commune. Sur le toit il y a une grande terrasse avec une belle vue sur la campagne.

<b>Prix par personne avec petit déjeuner* et 2x BBQ par semaine :</b>

- pour 2 personnes : 35 euros
- y compris 2x BBQ par semaine dans le surf camp
- y compris 1x réfrigérateur rempli ou buffet de petit-déjeuner dans le camp de surf
- Enfants jusqu'à 12 ans : 50 % de réduction
- Bébés jusqu'à 2 ans : gratuit

Cliquez ici pour [réservation]({{ links.de.onlineBooking.path }})
