---
tags: surfLodges
partial: casa-alda
---

Casa Alda est un appartement de vacances séparé au rez-de-chaussée avec sa propre terrasse et son espace barbecue. La maison se trouve dans la charmante ville de Casal da Murta, de l'autre côté de la route de campagne que vous devez traverser pour vous rendre à notre camp de surf (environ 400 m). Situé dans une rue calme, des chemins de terre mènent à de belles baies de baignade isolées (à seulement 1 km). L'appartement dispose d'une chambre double, d'une chambre à lits jumeaux, d'un salon avec télévision et accès Internet, d'une cuisine et d'une salle de bains.

<b>Prix par nuit/personne avec petit-déjeuner* et 2x BBQ par semaine :</b>

- pour 2 personnes : 50 euros
- pour 3 personnes : 40 euros
- à partir de 4 personnes : 30 EUR
- y compris 2x BBQ par semaine dans le surf camp
- y compris 1x réfrigérateur rempli ou buffet de petit-déjeuner dans le camp de surf
- Enfants jusqu'à 12 ans : 50 % de réduction
- Bébés jusqu'à 2 ans : gratuit

Cliquez ici pour [réservation]({{ links.fr.onlineBooking.path }})
