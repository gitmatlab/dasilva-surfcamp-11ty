---
tags: surfLodges
partial: people
---

## Gens

Notre surf camp Portugal est une maison ouverte, c'est-à-dire qu'il se passe toujours quelque chose chez nous. Outre les dîners partagés, qui sont inclus dans le prix, des barbecues spontanés ou des séances de feu de camp sont souvent organisés. Comme nous sommes nous-mêmes à moitié portugais, des amis à nous (locaux) viennent s'asseoir autour du feu de camp. Ensuite, il n'est pas rare que quelqu'un sorte soudainement une guitare et la soirée dure beaucoup plus longtemps que prévu. Mais il y a aussi de très belles destinations festives dans la région, vers lesquelles nous serions heureux de vous emmener dans notre navette. Après tout, vous ne devriez pas seulement apprendre à surfer, mais aussi vous familiariser avec la belle vie nocturne portugaise.
