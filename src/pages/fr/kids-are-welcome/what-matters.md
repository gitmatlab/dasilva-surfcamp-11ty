---
tags: kidsAreWelcome
partial: what-matters
---

Nous attachons une grande importance au fait que notre surf camp soit particulièrement adapté aux familles avec enfants. Les couples et les parents seuls sont entre de bonnes mains chez nous, quel que soit leur âge, avec leurs enfants.

Les adolescents s'intègrent rapidement au groupe d'autres invités qu'ils ont déjà rencontrés pendant le cours de surf. Après le cours de surf, nous avons du baby-foot, du ping-pong ou des fléchettes dans notre camp de surf, les gens cuisinent ensemble ou s'assoient autour du feu de camp et discutent agréablement. Les jeunes sont pleinement acceptés, de sorte que leurs parents peuvent même faire quelque chose seuls ou se coucher plus tôt que leurs enfants, car eux aussi s'amusent bien seuls.

C'est aussi très détendu pour les parents avec de jeunes enfants car nous avons une immense propriété, sans circulation de loin. Les enfants peuvent partir à l'aventure pendant que les parents laissent le soleil briller sur leur ventre. Nous avons même construit une petite piscine juste pour les enfants ! Et quand les enfants sont déjà au lit le soir, les parents peuvent encore s'asseoir dehors avec les autres ou prendre un verre de vin au bar.
Dans de nombreux cas, les enfants ont tellement aimé que les parents n'ont eu d'autre choix que de revenir l'année suivante. D'ailleurs, la nouvelle est déjà passée, ce qui veut dire que de plus en plus de gens viennent chez nous avec leurs enfants et certains ont déjà noué des amitiés qu'il faut renouveler avec nous chaque été.
