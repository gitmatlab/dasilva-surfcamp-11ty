---
tags: kidsAreWelcome
partial: discount
---

En termes de prix, nous essayons d'accommoder les parents en ne facturant rien pour les bébés jusqu'à deux ans. Nous fournissons gratuitement des lits bébé et pour les enfants jusqu'à 12 ans qui dorment avec leurs parents sur un lit supplémentaire dans la chambre, nous ne facturons que la moitié. Nous proposons des cours de surf pour les enfants à partir de 5 ans environ, à condition qu'ils sachent nager en toute sécurité et se sentent à l'aise dans l'eau. Malheureusement, nous ne pouvons pas offrir de réductions aux enfants sur les cours de surf. Pour les parents avec de jeunes enfants et de même niveau de surf, nous proposons qu'ils partagent un cours de surf afin que l'un d'eux puisse toujours s'occuper de l'enfant.