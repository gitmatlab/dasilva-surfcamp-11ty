---
tags: home
partial: spirit-of-surfing-2
---

### Apprenez à connaître »l'esprit du surf«,
### non seulement pendant les cours de surf, mais 24h/24 et 7j/7.
<br/>

Da Silva Surfcamp Portugal est une maison de campagne portugaise typique située dans un endroit calme avec une vue imprenable sur la campagne jusqu'à la mer. Un berger vient ici deux fois par jour avec son troupeau de moutons, le matin vous êtes réveillé par le chant d'un coq et sinon vous n'entendez que le chant des oiseaux. Vous pouvez marcher jusqu'à la plage en 20 minutes, avec le croiseur de plage, que vous pouvez emprunter gratuitement chez nous, cela ne prend que 10 minutes. Cette maison est parfaite pour les personnes qui veulent se déconnecter et échapper au stress de la grande ville. Il est idéal pour les familles avec enfants.