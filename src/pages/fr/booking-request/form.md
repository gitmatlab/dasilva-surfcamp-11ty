---
tags: bookingRequest
partial: form

contact:
  label : Vos coordonnées
firstName:
  label: prénom
lastName:
  label: nom de famille
address:
  label: adresse
phone:
  label: téléphone
  placeholder: +49 30 123 456 78
email:
  label: e-Mail
arrival:
  label: Arrivée
departure:
  label: Départ
travelers:
  label: Autres voyageurs 
evenMoreTravelers:
  hint: veuillez entrer d'autres participants dans le champ de commentaire ci-dessous. Veuillez également indiquer le nombre d'enfants (jusqu'à 12 ans) et de bébés (jusqu'à 2 ans) présents.

package:
  label: performances
surfPackage1Week:
  label: 1 semaine Surfpackage
mtbPackage1Week:
  label: 1 semaine MTB-Package
SurfMtbPackage1Week:
  label: 1 semaine Surf- & MTB-Package
surfPackage2Weeks:
  label: 2 semaines Surfpackage
mtbPackage2Weeks:
  label: 2 semaines MTB-Package
SurfMtbPackage2Weeks:
  label: 2 semaines Surf- & MTB-Package
BedAndBreakfastOnly:
  label: seulement Bed & Breakfast
surfPackageOnly:
  label: seulement cours de surf
mtbPackageOnly:
  label: seulement Mountainbike

bedAndBreakfast:
  label: Hébergement
multiBedRoom:
  label: Chambre partagée
doubleRoom:
  label: Chambre double
singleRoom:
  label: Chambre simple
suite:
  label: Suite  
tinyHouse:
  label: Tiny House
surfLodge:
  label: Surf Lodge  
camping:
  label: Camping

bookingOptions:
  label: options de réservation
airportTransfer:
  label: Nous aimerions être pris en charge à l'aéroport.
  hint: Conseils pour

comment:
  label: commentaire ou message

valueMissing:
  hint: veuillez préciser
patternMismatch:
  email: veuillez saisir votre e-mail

submit:
  label: soumettre

---

## Da Silva Surfcamp Portugal<br>Demande de réservation

Si vous souhaitez passer des vacances avec nous, veuillez remplir cette demande de réservation pour notre surf camp au Portugal.

{% include "booking-form.njk" %}
