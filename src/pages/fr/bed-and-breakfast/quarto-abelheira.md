---
tags: bedAndBreakfast
partial: quarto-abelheira
subtitle: Chambre double ou triple
---

Située également au premier étage de la maison principale, on peut voir, en regardant par la fenêtre de cette chambre, le village d'Abelheira à l'horizon. La chambre dispose d'un lit double et d'une mezzanine avec un matelas simple. Elle est donc parfaite pour un couple avec enfant ou trois adultes qui s'aiment beaucoup. La salle de bains du rez-de-chaussée est également partagée avec les deux autres chambres du premier étage.

Prix par personne, petit-déjeuner et 2 BBQ par semaine inclus :

- Juillet à septembre : 40 EUR
- Avril à juin et octobre : 30 EUR
- Occupation minimale : 2 personnes (adultes)
- Enfants jusqu'à 12 ans : 50% de réduction
- Bébés jusqu'à 2 ans gratuits

Cliquez ici pour [Réservation]({{ links.fr.onlineBooking.path }})
