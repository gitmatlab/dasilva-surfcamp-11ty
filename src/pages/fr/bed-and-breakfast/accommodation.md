---
tags: bedAndBreakfast
partial: accommodation
---

# Nos hébergements

Le Da Silva Surfcamp Portugal dispose d'une [chambre double](#quarto-zambujeira), d'une [chambre à trois lits](#quarto-abelheira), d'une [chambre avec terrasse](#quarto-do-terraco) pour 2 à 4 personnes, d'une [chambre à quatre lits](#quarto-azul), d'une [chambre à sept lits](#quarto-vermelho) et de [trois maisons en bois](#tiny-house-luna) séparées (petites maisons). Au total, nous avons de la place pour 32 personnes. Mais cela est assez bien réparti, car il existe de nombreux coins et niches où vous pouvez vous retirer, par exemple, si vous voulez lire un bon livre en paix.
