---
tags: bedAndBreakfast
partial: welcome
---

## Da Silva Surfcamp Portugal

<div class="h3">Um refúgio perfeito!</div>

### Une maison de campagne portugaise typique en pleine nature

<div class="h1">Bienvenue</div>

Le Da Silva Surfcamp Portugal est une maison de campagne portugaise typique en pleine nature sans aucune circulation à proximité. La propriété est entourée de vastes champs et de partout vous avez une vue fantastique sur le paysage verdoyant et une large vallée jusqu'aux villages d'en face. Confortablement allongé dans un hamac, vous pouvez voir la mer bleue et la station balnéaire **Praia da Areia Branca** depuis l'immense terrasse ensoleillée.

Le soir, il y a de magnifiques couchers de soleil ici. Il y a plusieurs beaux sentiers à travers la campagne jusqu'à la plage, qui se trouve à environ 20 minutes à pied. Cela prend moins de 10 minutes avec le croiseur de plage, que vous pouvez nous emprunter gratuitement, et vous pouvez vous y rendre en 3 minutes sur la route de campagne. Malgré la proximité de l'Atlantique, on a l'impression d'être quelque part dans la campagne loin de l'agitation de la vie balnéaire.
