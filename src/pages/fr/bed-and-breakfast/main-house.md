---
tags: bedAndBreakfast
partial: main-house
---

## Les chambres dans la maison principale

La chambre double, la chambre à 3 lits et la chambre en terrasse se trouvent au premier étage de la maison principale et partagent une salle de bain avec douche au rez-de-chaussée, la chambre en terrasse ayant encore des toilettes privées. Le Girls-4-Bedroom se trouve au rez-de-chaussée de la maison principale, juste à côté du confortable salon avec cheminée. Les lits simples ne peuvent être réservés que par des femmes. Mais bien sûr, tous les autres sexes sont également autorisés si vous réservez toute la chambre. La chambre à 7 lits se trouve dans une annexe à côté de la cuisine. Les chambres à plusieurs lits ont chacune leur propre salle de douche. La chambre à 7 lits dispose en plus d'une douche extérieure chaude et offre une petite kitchenette avec réfrigérateur.

La cuisine commune, où le petit déjeuner est servi chaque matin, se trouve au rez-de-chaussée, juste à côté du grand espace extérieur couvert, où ont également lieu les soirées barbecue.
