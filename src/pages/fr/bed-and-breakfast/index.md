---
permalink: /fr/hebergement/index.html
layout: bed-and-breakfast.njk
tags: bedAndBreakfast
locale: fr
navigation: Hébergement

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Surf et vacances actives pour la famille"

# max 158 characters
metaDescription: "Une maison portugaise, située en pleine nature, à proximité de superbes plages de surf et idéale pour les familles avec enfants."
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/bed-and-breakfast.jpg"
---
