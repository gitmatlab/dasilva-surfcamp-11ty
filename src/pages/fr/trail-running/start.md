---
tags: trailRunning
partial: start
---

# Trailrunning

<div class="h4" style="margin-top: -2rem;">Today's to do:</div>

<div class="h1" style="margin: 0 0 2rem 0;">Running</div>

La côte ouest du Portugal est sans aucun doute un paradis du surf. Des plages fantastiques et des spots de surf de première classe attirent de nombreux surfeurs du monde entier. Cependant, beaucoup ne savent pas encore que notre côte est aussi idéale pour le trail !

## Découvrez de près la côte portugaise

Nous proposons des [randonnées VTT]({{links[locale].mountainBike.path}}) guidées depuis 2017 et sommes donc toujours à la recherche de nouveaux itinéraires avec de superbes single trails dans notre région. Il arrive souvent que l'un ou l'autre single trail soit très difficile voire impossible à parcourir en VTT et donc inutilisable pour les randonnées VTT. Exactement ces sentiers sont parfaits pour le trail running! Directement de notre camp, il va directement aux falaises, où vous courrez d'une baie à l'autre sur des sentiers escarpés et rocheux le long de la côte et devez vous concentrer beaucoup pour ne pas être distrait par la vue fantastique sur les plages solitaires. et d'être distrait par le bleu de l'Atlantique. Mais il y a aussi quelques sentiers dans l'arrière-pays à travers des vallées solitaires et enchantées qui sont utilisées presque exclusivement par les chevriers et leurs troupeaux. Ici on se sent non seulement transporté dans un autre monde, mais même dans un autre temps...
