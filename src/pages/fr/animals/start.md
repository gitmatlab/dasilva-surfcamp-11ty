---
tags: animals
partial: start
---

# Bienvenue chez les animaux

**Nouveau en 2023 :**

Sur le terrain du camp de surf, nous avons maintenant une petite ferme avec des chèvres et des moutons pour vous et surtout nos petits hôtes. Les enfants sont invités à aller voir les animaux et à les nourrir à tout moment.

Malheureusement, les Happy Horses ne vivront plus avec nous dans le camp à la saison 2023, mais ont maintenant déménagé dans des pâturages plus grands avec leur propriétaire Christiane dans un village voisin.

Eliott et Timol sont toujours heureux d'avoir des enfants amoureux des chevaux et sont disponibles pour les invités du camp de surf 2 à 3 après-midi par semaine pendant deux heures chacun. Comme toujours, le programme de deux heures avec un maximum de 6 enfants comprend d'abord le nettoyage, les soins et la simulation des poneys ensemble, puis la selle est également pratiquée ensemble. Puis nous faisons des exercices de plomb pour connaître les poneys depuis le sol et Christiane nous explique ce qui est important dans la communication avec ces grands animaux. Après quelques figures du travail au sol, les enfants peuvent alors se balancer en selle à tour de rôle et, selon leur capacité, sont guidés par Christiane ou les autres enfants et apprennent de manière ludique à s'asseoir sur les poneys de manière équilibrée et détendue. ou pour rouler seul. De petites promenades dans la campagne environnante sont également possibles ici. Nous terminons le programme par le dessellement et les soins aux animaux et s'il reste un peu de temps, vous êtes les bienvenus pour aider un peu ou pour préparer le repas du soir.

Le programme est adapté aux enfants de 3 à 12 ans, mais l'équitation n'est possible que jusqu'à un poids maximum de 50 kg. Les enfants d'équitation avancés peuvent peut-être aussi rouler avec moi sur le terrain. Force de selle requise dans les 3 allures. Les enfants de moins de 5 ans doivent être accompagnés d'un parent. (Même s'ils n'ont pas à se tenir la main alors)