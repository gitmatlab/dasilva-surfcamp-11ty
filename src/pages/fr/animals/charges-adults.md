---
tags: animals
partial: chargesAdults
---

### Prix pour l'équitation<br/>(adultes)

## à partir de 25€ pour 1/2 heure

#### 1/2 h en longe : 25€ (carte 5 places 115€)

#### 1 h de manège : 35€ (carte 5 places 155€)

#### 1 h balade accompagnée : 38€ (carte 5 places 170€)

#### 2,5 h de promenade accompagnée : 70€ (carte 5 x 2 h 280€)
