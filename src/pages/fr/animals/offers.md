---
tags: animals
partial: offers
---

## Balades à cheval dans la région

Pour nos "grands cavaliers", nous sommes heureux d'organiser des cours d'équitation ou des excursions à cheval dans la "Quinta do Mosteiro", qui se trouve à seulement 7 minutes en voiture.

Jetez un oeil ici :
https://www.facebook.com/teamquintadomosteiro/
