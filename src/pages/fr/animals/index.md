---
permalink: /fr/animals/index.html
layout: animals.njk
tags: animals
locale: fr

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Animals"

# max 158 characters
metaDescription: "Promenades à cheval et à poney | Les gens et les chevaux s'amusent ensemble | Enfants et parents en vacances de surf au Portugal"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/animals.jpg"
---
