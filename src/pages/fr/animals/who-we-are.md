---
tags: animals
partial: who-we-are
---

Nous, c'est moi, **Christiane**, qui a perdu son cœur pour les chevaux quand elle était petite et qui rêve depuis d'avoir son propre cheval. Après une carrière professionnelle artistique, qui s'est terminée comme scénographe au cinéma, j'ai réalisé qu'à cause de tout le travail, quelque chose d'important était tombé à l'eau, et j'ai ramené les chevaux dans ma vie. Depuis, je sais ce qui manquait toujours.

Mon amour pour les chevaux a commencé quand j'étais enfant, et je suis donc très heureux de pouvoir enfin passer beaucoup de temps avec mes propres poneys et chevaux.

D'une manière ou d'une autre, j'ai toujours su que les chevaux sont intrinsèquement des animaux très affectueux et quoi qu'ils ** fassent **, ils ne ** font ** pas pour nous ** blesser **. C'est plutôt un miracle et un cadeau de la nature que ces grands animaux se joignent volontiers à nous et soient heureux d'être avec nous si nous les traitons gentiment et bien. Ce genre de maniabilité et une équitation fine et respectueuse du cheval sont ce que je souhaite aujourd'hui transmettre aux enfants, jeunes et adultes avec mes poneys et chevaux. Et bien sûr, le plaisir et la bonne humeur ne doivent pas être négligés.

**Jesuli**, mon hongre lusitanien, jeune de 7 ans et un très bon gars qui veut savoir exactement à qui il a affaire. Il a immédiatement regardé dans mon cœur.

**Timól**, un mélange Shetland de 9 ans qui m'est venu d'un très bon apprentissage avec un jeune garçon portugais. Il est un amoureux absolu des enfants et, en plus de l'équitation, il est également très amusant de travailler, de jouer ou de se promener avec lui depuis le sol.

**Eliott**, 10 ans noir et brun Shetty Pony vole tous les cœurs avec son nez mignon et sa nature amicale.