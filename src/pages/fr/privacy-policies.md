---
permalink: /fr/declaration-confidentialite/index.html
layout: plain-text.njk
tags: privacyPolicies
locale: fr
navigation: Protection des données

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Protection des données"

# max 158 characters
metaDescription: "Protection des données | Privacy Policies"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Protection des données/ declaration de confidentialite

Notre site Web peut généralement être utilisé sans fournir de données personnelles. Dans la mesure où des données personnelles (par exemple, nom, adresse ou adresses e-mail) sont collectées sur notre site Web, cela se fait toujours sur une base volontaire dans la mesure du possible. Ces données ne seront pas transmises à des tiers sans votre consentement exprès. Nous attirons votre attention sur le fait que la transmission de données sur Internet (par exemple lors de la communication par e-mail) peut présenter des failles de sécurité. Une protection complète des données contre l'accès par des tiers n'est pas possible. L'utilisation des données de contact publiées dans le cadre de l'obligation d'impression par des tiers pour envoyer de la publicité et du matériel d'information non sollicités est expressément interdite. Les exploitants du site se réservent expressément le droit d'engager des poursuites en cas d'envoi de publicités non sollicitées, telles que des spams.


### Déclaration de protection des données pour l'utilisation de Google Analytics

Ce site Web utilise Google Analytics, un service d'analyse Web fourni par Google Inc. (« Google »). Google Analytics utilise des "cookies", des fichiers texte qui sont stockés sur votre ordinateur et permettent une analyse de votre utilisation du site Web. Les informations générées par le cookie concernant votre utilisation de ce site Web (y compris votre adresse IP) sont transmises à un serveur de Google aux États-Unis et y sont stockées.

Google utilisera ces informations pour évaluer votre utilisation du site Web, pour compiler des rapports sur l'activité du site Web pour les opérateurs de site Web et pour fournir d'autres services liés à l'activité du site Web et à l'utilisation d'Internet. Google peut également transférer ces informations à des tiers si la loi l'exige ou si des tiers traitent ces données pour le compte de Google. Google n'associera en aucun cas votre adresse IP à d'autres données détenues par Google Inc.

Vous pouvez empêcher le stockage de cookies en configurant votre logiciel de navigation en conséquence ; nous attirons toutefois votre attention sur le fait que dans ce cas, vous ne pourrez éventuellement pas utiliser toutes les fonctions de ce site Internet dans leur intégralité. Vous pouvez également empêcher Google de collecter les données générées par le cookie et liées à votre utilisation du site Web (y compris votre adresse IP) et de traiter ces données par Google en téléchargeant le plug-in de navigateur disponible sous le lien suivant et en l'installant : https ://tools.google.com/dlpage/gaoptout?hl=de.


### Utilisation de Cloudflare comme CDN et de l'outil CAPTCHA Turnstile

Nous utilisons Cloudflare comme réseau de **distribution de contenu (CDN)** afin de garantir un accès plus rapide et plus fiable à notre site web. Cloudflare aide à optimiser les performances du site et à le protéger contre le trafic malveillant, y compris les attaques par déni de service distribué (DDoS). Dans ce cadre, Cloudflare peut collecter des données relatives à votre appareil et à votre activité de navigation afin d'améliorer ses services. Pour plus de détails sur la gestion de vos données par Cloudflare, veuillez consulter la [politique de confidentialité de Cloudflare](https://www.cloudflare.com/fr-fr/application/privacypolicy/).

En outre, nous utilisons le **CAPTCHA Turnstile** de Cloudflare pour protéger nos formulaires de contact et de réservation contre les abus automatisés et garantir que les interactions avec nos formulaires sont effectuées par de vrais utilisateurs. Cet outil peut collecter des informations telles que votre adresse IP et des données de navigation afin de vérifier que vous n'êtes pas un robot. Pour plus d’informations sur la gestion des données par Turnstile, veuillez consulter la politique de confidentialité de Cloudflare mentionnée ci-dessus.