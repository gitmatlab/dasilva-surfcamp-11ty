---
tags: mountainBike
partial: mtb-package
---

## MTB-Package

### à partir de 505 € par semaine/personne

<div class="h4">inclusif</div>

* 7 jours Bed & Breakfast dans notre camp
* 2x barbecue (également végétarien) avec feu de camp
* 5 circuits VTT (4-6 heures / 40-60 km par jour)
* Vélo de montagne CUBE de haut niveau (29 pouces) avec casque
* Navette vers les meilleurs sentiers VTT de notre région
