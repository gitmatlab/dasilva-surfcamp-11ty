---
tags: mountainBike
partial: bikes
---

## Nos VTT

Les prix de nos randonnées à vélo comprennent des VTT CUBE tout suspendus de haute qualité (Fully). Les modèles CUBE AMS, Sting et Stereo avec des roues de 29 pouces et un débattement de 120 mm (avant et arrière) sont disponibles.