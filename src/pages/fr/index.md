---
permalink: /fr/index.html
layout: home.njk
tags: home
locale: fr
navigation: Camp

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Surf et VTT et plus"

# max 158 characters
metaDescription: "Hébergement en maison de campagne portugaise | Ecole de Surf | Circuits VTT | Nature, plage et chevaux et beaucoup de plaisir pour les enfants"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---
