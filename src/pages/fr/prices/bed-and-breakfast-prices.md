---
tags: prices
partial: bedAndBreakfastPrices
---

- Basse saison
  - (Oct. – Juin)
  - Multiples : 25 €
  - Double : 30 €
  - Tiny House : 50 €
  - Tiny House Deluxe: 60 €
  - Simple : 55 €
- Haute saison
  - (Juillet – Sep.)
  - Multiples : 35 €
  - Double : 40 €
  - Tiny House : 60 €
  - Tiny House Deluxe: 80 €
  - Simple : 65 €
