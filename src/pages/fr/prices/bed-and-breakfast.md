---
tags: prices
partial: bedAndBreakfast
---

# Et combien coûte le plaisir ?

## Bed & Breakfast

### à partir de 25 € par jour / personne

<div class="h4">inclus :</div>

* Hébergement et petit-déjeuner buffet quotidien
* 2x barbecue par semaine (également végétarien) à partir d'un séjour d'une semaine
* Draps et serviettes
* Navette quotidienne vers la ville balnéaire la plus proche
* location gratuite de beach cruiser
* W-Lan gratuit dans le bâtiment principal
