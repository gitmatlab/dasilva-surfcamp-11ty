---
tags: prices
partial: surf-mtb-package-prices
---

- Basse saison
  - (Oct. – Juin)
  - Multiples : 505 €
  - Double : 540 €
  - Tiny house : 650 €
  - Tiny House Deluxe: 720 €
  - Simple : 715 €
- Haute saison
  - (Juillet – Sep.)
  - Multiples : 605 €
  - Double : 640 €
  - Tiny house : 735 €
  - Tiny House Deluxe: 875 €
  - Simple : 815 €
