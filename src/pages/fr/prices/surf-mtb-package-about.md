---
tags: prices
partial: surf-mtb-package-about
---

## Surf & MTB Package

### à partir de 505 € la semaine

L'alliance du surf et du VTT ! Idéal pour tous les surfeurs qui souhaitent également faire du VTT pendant leurs vacances et pour tous les vététistes qui souhaitent également surfer pendant leurs vacances. Le bon côté : vous pouvez planifier les journées pour pouvoir surfer quand les vagues sont bonnes et les autres jours vous pouvez partir sur les pistes VTT. 3 jours de surf, 3 jours de VTT, c'est plus possible en une semaine ?
