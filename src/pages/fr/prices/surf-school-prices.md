---
tags: prices
partial: surfSchoolPrices
---

- Basse saison
  - (Oct. – Juin)
  - 1 jour /<br/>2 séances : 65 €
  - 5 jours /<br/>10 séances : 240 €
  - 10 jours /<br/>20 séances : 460 €
- Haute saison
  - (Juillet – Sep.)
  - 1 jour /<br/>2 séances : 60 €
  - 5 jours /<br/>10 séances : 260 €
  - 10 jours /<br/>20 séances : 490 €
