---
tags: prices
partial: surfPackage2Weeks
---

## Surf Package 2 semaines

### à partir de 750€ par personne

<div class="h4">inclus :</div>

* 14 nuits Bed & Breakfast (comme décrit ci-dessus)
* 20 cours de surf intensif de 2 heures<br/>(2 heures par session / 2 sessions par jour)
* équipement de surf complet pendant le cours<br/>(planche de surf, leash, combinaison)
* Navette pour les plages avec les meilleures vagues de notre région
