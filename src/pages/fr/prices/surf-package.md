---
tags: prices
partial: surfPackage
---

## Surf-Package

### à partir de 395 € par semaine/personne

<div class="h4">inclus :</div>

* 7 nuits Bed & Breakfast (comme décrit ci-dessus)
* 10 cours de surf intensif de 2 heures<br/>(2 heures par session / 2 sessions par jour)
* équipement de surf complet pendant le cours<br/>(planche de surf, leash, combinaison)
* Navette pour les plages avec les meilleures vagues de notre région
