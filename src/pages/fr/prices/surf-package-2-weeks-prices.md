---
tags: prices
partial: surfPackage2WeeksPrices
---

- Basse saison
  - (Oct. – Juin)
  - Multiples :  euros
  - Double : 820 €.
  - Petite maison : 1160 €
  - Tiny House Deluxe: 1.300 €
  - Simple : 1170 €.
- Haute saison
  - (Juillet – Sep.)
  - Multiples : 840 euros
  - Double : 910
  - Tiny House : 1.330 €
  - Tiny House Deluxe: 1.610 €
  - Simple : 1.470 €.
