---
tags: prices
partial: surfPackagePrices
---

- Basse saison
  - (Oct. – Juin)
  - Multiples : 395 €
  - Double : 430 €
  - Tiny house : 590 €
  - Tiny House Deluxe: 660 €
  - Simple : 605 €
- Haute saison
  - (Juillet – Sep.)
  - Multiples : 450 €
  - Double : 485 €
  - Tiny house : 680 €
  - Tiny House Deluxe: 820 €
  - Simple : 695 €
