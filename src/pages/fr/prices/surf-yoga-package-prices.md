---
tags: prices
partial: surfYogaPackagePrices
---

- Basse saison
  - (Oct. – Juin)
  - Multiples : 455 €
  - Double : 490 €
  - Tiny House : 690 €
  - Tiny House Deluxe: 760 €
  - Simple : 665 €
- Haute saison
  - (Juillet – Sep.)
  - Multiples : 510 €
  - Double : 545 €
  - Tiny House : 780 €
  - Tiny House Deluxe: 920 €
  - Simple : 755 €
