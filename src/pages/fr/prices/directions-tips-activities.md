---
tags: prices
partial: directions-tips-activities
---

## Jour d'arrivée et de départ

En théorie, vous pouvez arriver et repartir n'importe quel jour de la semaine. Cependant, comme les cours de surf commencent toujours le lundi et que nous avons notre fameux barbecue de bienvenue le dimanche soir, il est conseillé d'arriver et de partir le dimanche, notamment en raison de la dynamique de groupe qui se développe automatiquement au cours d'une semaine.

En parlant de comment vous pouvez nous trouver, vous pouvez le découvrir ici: [Arrivée]({{links.fr.travelDirections.path}}).

## Croiseur de plage

Pour vous rendre à la plage ou simplement pour vous déplacer en voiture, vous pouvez emprunter gratuitement chez nous un beach cruiser chic à tout moment. C'est cool et ça détend les jambes :o)

## Aliments

Dans la ville balnéaire de Praia da Areia Branca (2 km), il y a un petit supermarché et un marché hebdomadaire quotidien avec des fruits et légumes frais, du poisson, etc. On peut y trouver tout ce dont on a besoin. Dans la ville de Lourinha (4 km), il y a de plus grands supermarchés (par exemple LIDL et ALDI), où il y a un choix encore plus grand et qui sont ouverts tous les jours jusqu'à 21 h. Si vous n'avez pas de voiture, les membres de notre équipe vous emmèneront faire vos courses. Par ailleurs, vous trouverez partout de nombreux cafés et restaurants bon marché, où vous pourrez déguster de délicieux plats typiquement portugais. Mais il y a aussi une pizzeria, un très bon restaurant indien sur la plage et un fast-food sur la route principale. Par ailleurs, il est possible de commander de la nourriture directement au surf camp. Nos équipiers sont également ravis de faire une commande collective pour tous les clients et d'aller chercher la nourriture pour vous.
