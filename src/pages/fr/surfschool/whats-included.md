---
tags: surfschool
partial: whats-included
---

### 1 semaine / 5 stages de surf comprenant :

* 10 cours de surf intensif de 2 heures (2 heures par session / 2 sessions par jour)
* Équipement de surf complet (planche de surf, leash, combinaison) pendant le cours
* Navette pour les plages avec les meilleures vagues de notre région
* Barbecue final avec feu de camp dans le camp de surf
