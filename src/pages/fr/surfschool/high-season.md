---
tags: surfschool
partial: high-season
---
### Haute saison

#### *juillet - septembre*

#### ~

#### 1 journée / 2 séances : 60 €

#### 5 jours / 10 séances : 240 €

#### 10 jours / 20 séances : 420 €
