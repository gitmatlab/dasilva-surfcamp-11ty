---
tags: surfschool
partial: class
---

## Débutant (première semaine)

* Théorie sur la météo, les marées, les courants et les vagues
* Introduction à la matière (matériel de surf)
* Les règles de sécurité
* Règles de priorité dans l'eau
* Pagayer, s'asseoir, tourner
* Surfer la partie blanche de la vague

## Avancé (deuxième semaine)

* Surfer la partie verte de la vague
* Duck Dive (plongée à travers la vague)
* premières manœuvres de surf
* technologie avancée
* Surfer avant et arrière
* Amélioration du style

Au fait, vous avez également la possibilité de combiner l'offre de l'école de surf Da Silva avec des visites guidées en VTT. Ça semble intéressant? Plus d'informations à ce sujet ici, sur : [{{ links[locale].mountainBike.title }}]({{ links[locale].mountainBike.path }})
