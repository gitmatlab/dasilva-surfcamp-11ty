---
tags: surfschool
partial: qualified
---

Depuis 2016, le Da Silva Surfcamp organise les cours de surf en interne avec l'école de surf interne de la Da Silva Surfschool et aide les débutants ainsi que les surfeurs avancés sur la planche.

En fonction des conditions de vagues et du niveau des élèves, elle le fait sur différents spots de surf à moins de 20 minutes en voiture de Praia da Areia Branca. On veille à ce que les plages ne soient pas aussi fréquentées que les plages bien connues de Peniche.

Les cours de surf se déroulent généralement du lundi au vendredi. Cependant, il peut arriver que l'une ou l'autre session doive être annulée en raison des conditions météorologiques ou des vagues. Dans ce cas, des activités alternatives telles que des excursions dans les environs, des sessions de SUP, des sessions de skateboard, etc. seront proposées.

A la fin de l'ensemble du cours (débutants et/ou avancés), chaque élève doit être capable de se tenir debout sur la planche (décollage) ou de surfer dans la partie ininterrompue de la vague (eau verte). Les succès individuels dépendent fortement de la motivation, du talent et de la condition athlétique générale de l'élève. Le prix comprend la navette en bus, la planche de surf et la combinaison, les cours théoriques et pratiques et l'assurance.
