---
tags: mtbTourMontejunto
partial: data
---

- Strecke 39 km
- Reine Fahrzeit 4:15 h
- Höhenmeter 1.213 m
- [![Strava link](/_assets/mtb-tours/strava.jpg)](https://strava.app.link/WnzBsjcBLZ)
