---
tags: mtbTourSocorro
partial: data
---

- Strecke 42 km
- Reine Fahrzeit 4:35 h
- Höhenmeter 1.169 m
- [![Strava link](/_assets/mtb-tours/strava.jpg)](https://strava.app.link/LCL8AVXoNZ)
