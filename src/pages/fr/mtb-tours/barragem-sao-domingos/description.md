---
tags: mtbTourBarragemSaoDomingos
partial: description
---

Die fünfte und letzte Tour führt uns vom Da Silva Bikecamp an der Küste entlang bis nach Consolação. Nach der gestrigen Tour zum Montejunto lassen wir es heute etwas langsamer angehen.

Auf Feldwegen und Trails folgen wir der Küste, mit zwei kurzen Abstiegen ans Wasser, in nördlicher Richtung bis zum Praia da Consolação. Von hier fahren wir landeinwärts nach Atouguia da Baleia. Gegenüber der eindrucksvollen Kirche Nossa Senhora da Conceição legen wir Rast ein. Zur Stärkung gibt es einen Bica und ein paar eingelegte Tremoços (Lupinenkerne).

Bei der anschließenden Runde durch den Ort sehen wir noch die zweite Kirche Igreja de São Leonardo und merken einmal mehr, wie Haushunde hier sowohl als „Alarmanlage“ als auch als „Türglocke“ dienen – zum Glück nur rein akustisch.

Entlang des Stausees geht es in südlicher Richtung zurück, hindurch zwischen den Kürbisfeldern und über kleine Trails. Den zweiten Teil des Hometrails (von unserer Runde am ersten Tag) hätten wir hier noch anfügen können, entscheiden uns aber, rechtzeitig zum BBQ zurück im Camp zu sein und die ebenso vielfältige wie fordernde Woche auf den Fullies ausklingen zu lassen.
