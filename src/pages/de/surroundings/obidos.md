---
tags: surroundings
partial: obidos
mapLink: https://goo.gl/maps/sqdvM1BTgng34ARv5
---

Ein mittelalterliches Städtchen (25km nördlich) mit riesiger Stadtmauer innerhalb derer keine Autos fahren dürfen. Im August gibt es ein tolles Mittelalterfest.
