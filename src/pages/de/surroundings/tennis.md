---
tags: surroundings
partial: tennis
webLink: https://goo.gl/maps/xG78dSqgVvQbYNHe6
mapLink: https://bit.ly/2GzYYaH
---

### Tennis in Praia da Areia Branca

Nicht immer nur im Neo schwitzen? Stimmt, geht natürlich auch auf dem Tennisplatz in Praia da Areia Branca. Locker-flockig servieren und im Einzel oder Doppel mit den Skills in der Sonne glänzen! Huah! Uff! Yeah!
