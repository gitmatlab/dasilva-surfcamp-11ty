---
permalink: /de/surf-bar/index.html
layout: surf-bar.njk
tags: surfBar
locale: de
navigation: Surf Bar

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Da Silva Surf Bar"

# max 158 characters
metaDescription: "Saufen, Feiern, Nachtleben"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-bar.jpg"
---
