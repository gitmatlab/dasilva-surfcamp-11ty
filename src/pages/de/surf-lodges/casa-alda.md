---
tags: surfLodges
partial: casa-alda
---

Casa Alda ist eine separate Ferienwohnung im Erdgeschoss mit eigener Terrasse und Grillstelle. Das Haus liegt im gemütlichen Ort Casal da Murta auf der anderen Seite der Landstraße, die man überqueren muss, wenn man zu unserem Surfcamp möchte (ca. 400m). In einer ruhigen Seitenstraße gelegen kommt man von hier über Feldwege zu wunderschönen einsamen Badebuchten (nur 1 km). Die Wohnung hat ein Schlafzimmer mit Doppelbett, ein Schlafzimmer mit zwei Einzelbetten, Wohnzimmer mit Fernseher und Internetzugang, Küche und Badezimmer.

<b>Preise pro Nacht/Person inkl. Frühstück* und 2x BBQ pro Woche:</b>

- für 2 Personen: 50 EUR
- für 3 Personen: 40 EUR
- ab 4 Personen: 30 EUR
- inkl. 2x BBQ pro Woche im Surfcamp
- inkl. 1x gefüllter Kühlschrank oder Frühstücksbuffett im Surfcamp
- Kinder bis 12 Jahre: 50% Rabatt
- Babys bis 2 Jahre: kostenlos

Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
