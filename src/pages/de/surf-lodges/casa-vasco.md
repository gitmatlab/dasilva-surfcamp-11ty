---
tags: surfLodges
partial: casa-vasco
---

Goreti Vasco hat mit ihrer Familie schon viele Jahre in Deutschland gelebt. Nun hat sie sich den Traum vom eigenen Haus in Portugal erfüllt und vermietet zwei Ferienwohnungen in 400m Entfernung von unserem Surfcamp. Sie freut sich über Gäste, mit denen sie sich auf deutsch unterhalten kann. Macht man von hier einen gemütlichen Spaziergang über die Feldwege, erreicht man die nahegelegenen Badestrände in kurzer Zeit. Den Gästen stehen der Innenhof und eine kleine Wiese mit Blick über das Land zur Verfügung. Die Wohnung besteht aus einer großen Wohnküche mit Kamin und Fernseher, zwei Schlafzimmern mit Doppelbett (jeweils 135 x 185 cm) und einem Badezimmer mit Badewanne. Außerdem gibt es noch eine zweite Küche mit Feuerstelle, wo man gut grillen kann.

<b>Preise pro Nacht inkl. Frühstück und 2x BBQ pro Woche im Surfcamp:</b>

- für 2 Personen: 110 EUR
- für 3 Personen: 135 EUR
- für 4 Personen: 160 EUR
- Mindestbelegung: 2 Personen (Erw.)
- Babys bis 2 Jahre: kostenlos

Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
