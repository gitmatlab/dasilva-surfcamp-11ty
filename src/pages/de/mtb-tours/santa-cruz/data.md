---
tags: mtbTourSantaCruz
partial: data
---

- Strecke 34 km
- Reine Fahrzeit 3:22 h
- Höhenmeter 702 m 
- <a href="https://strava.app.link/kQdmMHOCHZ" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>
