---
tags: mtbTourDasilvaHomeTrail
partial: description
---

Wir starten direkt im Da Silva Surf & Bikecamp. Nach zwei Minuten Fahrt wartet die erste kleine Mutprobe: Ein steiles Stück Hang zu den Feldern hinunter. Keine großen Hindernisse und reichlich Auslauf und das Biker-Herz schlägt gleich schneller. Bremsen auf und runter!

Auf Feldwegen geht es am nächsten Dorf vorbei Richtung Osten und ab in die Hügel. Der Untergrund ist mal sandig, mal steinig. Zwischen Bäumen hindurch fahren wir mal querfeldein und es scheint, als fände Daniel den Weg auch mit verbundenen Augen.

Unterhalb eines Steinbruchs, durch den wir später noch eine spannende Runde drehen werden, führt uns der Trail nach Pena Seca. Ein Wandbild bietet Daniel den Anlass zu erzählen, was es mit dem Namen des Ortes auf sich hat.

Von hier aus bleiben wir etwa 10 Kilometer auf der gleichen Höhe. Verschiedene Trails führen uns zwischen Steinmauern auf alten Pfaden durch buschiges Hinterland und kleine Wäldchen. Die Trails sind abwechslungsreich: mal glatter und flott zu fahren, dann wieder felsig und eher technisch, aber ohne große Steigungen. Die Pause in Moledo ist danach ebenso willkommen wie das alkoholfreie Sagres. Auch zu diesem Ort gibt es eine Geschichte, die Daniel zu erzählen weiß.

Hinter Moledo geht es weitere 10 Kilometer über Trails und Feldwege, bis uns der nächste Anstieg einen Panoramablick über Lourinha und die Gegend verschafft. Nach einer schnellen Abfahrt erreichen wir die Stadt. Richtung Küste erwartet uns jetzt eine kurze Durststrecke bergauf, für die wir mit einer grandiosen Aussicht auf die Bucht von Praia Areia Branca und den Atlantik belohnt werden. Auf flowigen Trails geht es Richtung Strand. Die kurze Pause unmittelbar am Meer schafft einen tollen Kontrast zu den verschlungenen Trails im Hinterland.

Der Aufstieg zurück ins Camp erfordert noch eine kurze Anstrengung, aber dank der Szenerie sind die müden Beine schnell vergessen.
