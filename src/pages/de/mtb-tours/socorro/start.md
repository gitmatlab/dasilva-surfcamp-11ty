---
tags: mtbTourSocorro
partial: start
---

# Cucos – Serra do Socorro

Anspruchsvolle Tour mit Downhill-Passagen (S2-S3) in der Gegend von Torres Vedras, ca. 25 km südlich von unserem Camp.
