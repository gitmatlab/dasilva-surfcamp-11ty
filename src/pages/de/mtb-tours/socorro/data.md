---
tags: mtbTourSocorro
partial: data
---

- Strecke 42 km
- Reine Fahrzeit 4:35 h
- Höhenmeter 1.169 m
- <a href="https://strava.app.link/LCL8AVXoNZ" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>
