---
tags: prices
partial: bedAndBreakfastPrices
---

- Low Season
  - (October – June)
   - Multiple: 25 €
  - Double: 30 €
  - Suite: 35 €
  - Tiny House: 50 €
  - Tiny House Deluxe: 60 €
  - Single: 55 €
- High Season
  - (July – September)
  - Multiple: 35 €
  - Double: 40 €
  - Suite: 45 €
  - Tiny House: 60 €
  - Tiny House Deluxe: 80 €
  - Single: 65 €
