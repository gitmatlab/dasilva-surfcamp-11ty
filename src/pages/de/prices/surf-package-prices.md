---
tags: prices
partial: surfPackagePrices
---

- Low Season
  - (October – June)
  - Multiple: 415 €
  - Double: 450 €
  - Suite: 485 €
  - Tiny House: 590 €
  - Tiny House Deluxe: 660 €
  - Single: 625 €
- High Season
  - (July – September)
  - Multiple: 505 €
  - Double: 540 €
  - Suite: 575 €
  - Tiny House: 680 €
  - Tiny House Deluxe: 820 €
  - Single: 715 €
