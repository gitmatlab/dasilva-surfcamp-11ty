---
tags: yoga
partial: start
---

## Besser surfen und wohlfühlen durch Yoga

Finde bei uns die Entschleunigung, den Fokus und die Ruhe, die du brauchst, um ganz bei dir selbst anzukommen. Unsere Yoga-Sessions bieten dir den perfekten Ausgleich zum Surfen und stärken deinen Körper für eine gelungene Surf-Session. 

Yoga und Surfen sind wie füreinander geschaffen - während du beim Surfen voll in Aktion bist, findest du beim Yoga deine innere Ruhe.

Wir bieten einen Mix aus klassischem Hatha Yoga und modernem Vinyasa Flow, der auf jedes Level zugeschnitten ist. Egal, ob du Anfänger oder schon ein erfahrener Yogi bist - bei uns findest du die passenden Sessions für dich.

Wir möchten dazu beitragen, dass du deine Gesundheit förderst und deinen Geist, Körper und Natur in Einklang und Harmonie bringst.
