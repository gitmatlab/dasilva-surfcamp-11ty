---
tags: bedAndBreakfast
partial: tiny-house-jorgina
---

Das Holzhäuschen eignet sich ideal für Leute, die gern am geselligen Treiben eines Surfcamps teil haben möchten, sich aber jederzeit zurückziehen können möchten. Das Häuschen hat ein sehr angenehmes Raumklima. Tagsüber wird es nicht zu heiss und nachts hält sich die angenehme Wärme der Sonne im Haus. Wenn man etwas kochen möchte, steht einem die Gemeinschaftsküche des Surfcamps zur Verfügung. Auf dem Weg dorthin kommt man direkt an unserer Bar vorbei, wo man sich vor oder nach der anstrengenden Küchenarbeit erfrischen kann.

Preise pro Person und Nacht inkl. Frühstück und 2x BBQ pro Woche:

- Juli bis September: 60 EUR
- Oktober bis Juni: 50 EUR
- Mindestbelegung: 2 Personen (Erw.)
- Kinder bis 12 Jahre: 50% Rabatt
- Babys bis 2 Jahre kostenlos

Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
