---
tags: animals
partial: start
---

# Animals

**Neu in 2023:**

Auf dem Gelände des Surfcamps haben wir jetzt für euch und insbesondere unsere kleinen Gäste eine kleine Farm mit Ziegen und Schafen. Die Kinder dürfen gerne jederzeit zu den Tieren und diese auch füttern.


Die Happy Horses werden in der Saison 2023 leider nicht mehr bei uns im Camp leben sondern sind nun auf größere Weiden zu ihrer Besitzerin Christiane in einem benachbarten Dorf gezogen.


