---
tags: kidsAreWelcome
partial: discount
---

Preislich versuchen wir, den Eltern etwas entgegen zu kommen, in dem wir für Babys bis zwei Jahre gar nichts berechnen. Babybetten stellen wir kostenlos zur Verfügung und für Kinder bis 12 Jahre, die mit ihren Eltern im Zimmer auf einem Zustellbett schlafen, berechnen wir nur die Hälfte. Surfkurse bieten wir schon für Kinder ab ca. 5 Jahre an, sofern sie einigermaßen sicher schwimmen können und sich im Wasser wohlfühlen. Leider können wir bei den Surfkursen keinen Nachlass für Kinder anbieten. Für Eltern mit Kleinkindern und dem gleichen „Surflevel“ bieten wir an, dass sie sich einen Surfkurs teilen, damit immer einer von beiden auf das Kind aufpassen kann.