---
tags: skateboarding
partial: skatePark
---

Wer das volle Programm gleich um die Ecke haben möchte, dem sei der Skatepark Lourinhã wärmstens ans Skater-Herz gelegt – nur 3 km südlich von Praia da Areia Branca warten auf euch sweete Rampen, Treppen, Geländer, Bänke & feine Pipes.

<a href="https://bit.ly/2TOFqXt" target="_blank"><img src="/_assets/icons/svgrepo/google-maps.svg" class="surroundings-cards__map-link" alt="google maps link"></a>
