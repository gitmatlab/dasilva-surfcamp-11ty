---
permalink: /de/skateboarding/index.html
layout: skateboarding.njk
tags: skateboarding
locale: de
navigation: Skateboarding

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Skateboarding"

# max 158 characters
metaDescription: "Eigene Miniramp auf dem Camp | Skatepark Lourinhã | Surfing & Skateboarding"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/skateboarding.jpg"
---
