---
permalink: /de/datenschutzerklaerung/index.html
layout: plain-text.njk
tags: privacyPolicies
locale: de
navigation: Datenschutzerklärung

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Datenschutzerklärung"

# max 158 characters
metaDescription: "Datenschutzerklärung | Privacy Policies"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Datenschutzerklärung

Die Nutzung unserer Webseite ist in der Regel ohne Angabe personen-bezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben. Wir weisen darauf hin, dass die Datenübertragung im Internet (z. B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich. Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.


### Datenschutzerlärung für die Nutzung von Google Analytics

Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. (“Google”). Google Analytics verwendet sog. “Cookies”, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglicht. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich Ihrer IP-Adresse) wird an einen Server von Google in den USA übertragen und dort gespeichert.

Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben ist oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten der Google Inc. in Verbindung bringen.

Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: http://tools.google.com/dlpage/gaoptout?hl=de.


### Verwendung von Cloudflare als CDN und Turnstile CAPTCHA-Tool

Wir verwenden Cloudflare als unser **Content Delivery Network** (CDN), um einen schnelleren und zuverlässigeren Zugriff auf unsere Website zu gewährleisten. Cloudflare hilft dabei, die Leistung der Website zu optimieren und sie vor schädlichem Datenverkehr, einschließlich Distributed Denial of Service (DDoS)-Angriffen, zu schützen. Dabei kann Cloudflare Daten zu Ihrem Gerät und Ihrem Surfverhalten erfassen, um seine Dienste zu verbessern. Weitere Informationen zur Datenverarbeitung durch Cloudflare finden Sie in der [Datenschutzrichtlinie von Cloudflare](https://www.cloudflare.com/de-de/application/privacypolicy/).

Zusätzlich setzen wir das **Turnstile CAPTCHA** von Cloudflare ein, um unsere Formulare für Kontakt- und Buchungsanfragen vor automatisiertem Missbrauch zu schützen und sicherzustellen, dass Interaktionen mit unseren Formularen von echten Nutzern stammen. Dieses Tool kann Informationen wie Ihre IP-Adresse und Browserdaten erfassen, um zu überprüfen, ob Sie kein Bot sind. Weitere Informationen zur Datenverarbeitung durch Turnstile finden Sie in der oben verlinkten Datenschutzrichtlinie von Cloudflare.
