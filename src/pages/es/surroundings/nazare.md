---
tags: surroundings
partial: nazare
mapLink: https://goo.gl/maps/CqsQGckf1wFVMJVJA
---

Die weltberühmten Monsterwellen bekommt man hier (60km nördlich) nur außerhalb der Saison zu sehen, aber der Ort ist auch sonst sehr schön und absolut sehenswert.
