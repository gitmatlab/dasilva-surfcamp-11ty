---
tags: surroundings
partial: museu-lourinha
webLink: https://museulourinha.org
mapLink: https://bit.ly/2D59BjA
---

### Museu da Lourinhã

Zeitreise gefällig? Dann lasst euch die spannende Geschichte der Gegend um Lourinhã erzählen. Inklusive Dinoskelett, dem Leben und der Arbeit der Menschen und faszinierender Handwerkskunst verschiedenster Epochen.
