---
tags: surroundings
partial: peniche
mapLink: https://goo.gl/maps/MCEuLzm2VbhXNXXW9
---

Eine authentische Fischerstadt (12 km nördlich) mit alter Burg am Hafen und vielen Fischrestaurants. Die Inselgruppe „Berlengas“ erreicht man mit der Fähre in einer Stunde.
