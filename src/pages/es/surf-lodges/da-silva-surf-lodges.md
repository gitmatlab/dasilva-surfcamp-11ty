---
tags: surfLodges
partial: da-silva-surf-lodges
---

Die "Da Silva Surf Lodges" bieten wir euch als exklusive Alternative zu unserem Surfcamp an. Nur 300m von uns entfernt, kann man hier in seinen Urlaub in idyllischem Ambiente genießen. Zur Verfügung stehen fünf Apartments mit einem Schlafzimmer, großzügigem Wohnbereich mit offener Küche und komfortablen Badezimmer. Das sechste Apartment hat zwei Schlafzimmer und zwei Badezimmer (Preise auf Anfrage). Smart-TV, W-Lan, Geschirrspüler sind selbstverständlich. Vor dem Gebäude befindet sich ein kleiner See, in den eine natürliche Quelle plätschert. Das Holzpavillon über dem See bietet sich hervorragend dafür an, ein gutes Buch zu lesen, während man die Vögel zwitschern hört :)

**Preise pro Person und Nacht inkl. Frühstück und 2x BBQ pro Woche:**

**Juli bis September**

- für 2 Personen: 60 EUR
- für 3 Personen: 50 EUR
- ab 4 Personen: 40 EUR

**Oktober bis Juni**

- für 2 Personen: 50 EUR
- für 3 Personen: 40 EUR
- ab 4 Personen: 35 EUR

**Konditionen**

- Mindestbelegung: 2 Personen (Erw.)
- Babys bis 2 Jahre: kostenlos

Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
