---
tags: surfLodges
partial: quarto-por-do-sol
---

Der Innenhof von Casa Francisco und Studio Prata bietet als dritte Unterkunft das Quarto Por do Sol für zwei Personen an. Der Name bedeutet "Zimmer Sonnenuntergang", da hier ab mittags die Sonne durch das Fenster scheint und man am Abend traumhafte Sonnenuntergänge sehen kann. Den Gästen steht ein Zimmer mit Doppelbett, kleine Küchenzeile, Esstisch, Smart-TV und Duschbad zur Verfügung. Im gemeinsam genutzten Innenhof sind Gartenmöbel und eine Grillstelle vorhanden. Auf dem Dach gibt es eine große Terrasse mit schönem Blick über das Land. 

<b>Preise pro Person inkl. Frühstück* und 2x BBQ pro Woche:</b>

- für 2 Personen: 35 EUR
- inkl. 2x BBQ pro Woche im Surfcamp
- inkl. 1x gefüllter Kühlschrank oder Frühstücksbuffett im Surfcamp
- Kinder bis 12 Jahre: 50% Rabatt
- Babys bis 2 Jahre: kostenlos

Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
