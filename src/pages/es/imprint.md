---
permalink: /es/impressum/index.html
layout: plain-text.njk
tags: imprint
locale: es
navigation: Impressum

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Impressum"

# max 158 characters
metaDescription: "Impressum und Adresse"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Impressum

### Inhaber/ Herausgeber

```
Daniel Wohlang da Silva
Da Silva Surfcamp Portugal
Casal da Capela, Casal da Murta
Rua Pôr do Sol, N° 21
2530 – 077 Lourinhã - Portugal
Mobiltelefon: +351 913 818 750
(Anruf / WhatsApp / Signal)
Telefon (Festnetz): +351 261 461 515
E-Mail: surfcamp[@]dasilva.de
```

Verantwortlich gemäß § 55 Abs. 2 RStV: Daniel Wohlang da Silva

### Urheberrecht

Die durch den Seitenbetreiber erstellten Inhalte und Werke sind urheberrechtlich geschützt. Dies betrifft die Gestaltung wie den Inhalt. Vervielfältigungen, Bearbeitungen oder Veränderungen sind nur nach Erteilung einer schriftlichen Genehmigung erlaubt. Ebenso bleiben alle Rechte für Wiedergaben, in welcher Form und in welchem Medium auch immer, vorbehalten.

### Haftungsausschluß

Eigene Inhalte | Sämtliche Informationen werden regelmäßig und sorgfältig, nach bestem Wissen und Gewissen, aktualisiert. Im rechtlichen Sinne sind sie jedoch ohne Gewähr.
Fremde Inhalte | An manchen Stellen verweisen wir auf fremde Seiten (Links). Auf deren Gestaltung, Inhalte, Aktualität, Vollständigkeit und Korrektheit haben wir keinerlei Einfluß. Deshalb distanzieren wir uns ausdrücklich von allen Inhalten dieser Seiten, sowie von allen Seiten, zu denen wiederum die von uns gelinkten Seiten führen.

# Licences

### Font Awesome

We use some social icons and other playful icons from **Font Awesome** with the **Creative Commons Attribution 4.0 International** license and requires that we link to the <a href="https://fontawesome.com/license" target="_blank">Font Awesome Licens</a>. Thank you for the wonderful icons. We pretty much appreciate.
