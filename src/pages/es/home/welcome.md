---
tags: home
partial: welcome
---

<div class="h3" style="margin:0;">The Portuguese Spirit of Surfing</div>
<div class="h4" style="margin:0 0 -1.6rem 0;">im</div>

<h1 style="margin:3.2rem 0 0 0">DA SILVA SURFCAMP</h1>
<h2 style="margin:0 0 1.6rem 0">Portugal</h2>

Im Da Silva Surfcamp ist man ganz nah an der Natur, am Meer und an der portugiesischen Kultur und Lebensfreude. Erstklassige Surfspots mit wenig Crowd bieten perfekte Bedingungen für Anfänger und Fortgeschrittene. Legendäre BBQs und spontane Lagerfeuer-Sessions sorgen für unvergessliche Momente!

