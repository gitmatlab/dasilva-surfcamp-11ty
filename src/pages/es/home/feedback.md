---
tags: home
partial: feedback
---

### Und das schreiben unsere Gäste:

"Wir hatten eine mega Zeit im Da Silva Surfcamp - das lässige Gelände mit viel Charme und Kreativität gestaltet bietet den idealen Raum für die Erholung der ganzen Familie: Surfsessions an den zahlreichen Spots in nächster Umgebung, ausgiebige Beachtage, Chill-out im Camp, Yoga, Ponyreiten, Skaten, Lagerfeuer unterm Sternenhimmel, gesellige Barabende mit fetzigen Kickerturnieren, das Frühstück mit glutenfreien & veganen Optionen, leckere BBQ-Abende und einfach entspannte Leute - wir reisen mit unzähligen schönen Erinnerungen im Gepäck ab. Was die Zeit für uns aber vor allem geprägt hat, war das fünf Sterne allererste Sahne Team 2022, durch das das Da Silva Surfcamp für uns zum absoluten Happy Place wurde - Danke an alle fürs Verwöhnen - wir haben (nicht nur einmal :-) verlängert und kommen gerne wieder!"
