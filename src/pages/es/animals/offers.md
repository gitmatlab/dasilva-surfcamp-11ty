---
tags: animals
partial: offers
---

## Ausritte in der Umgebung

Für unsere "großen Reiter" vereinbaren wir gerne Reitstunden oder Reitausflüge in der „Quinta do Mosteiro“, welche nur 7 Minuten mit dem Auto von uns entfernt ist.

Schaut doch mal hier:
https://www.facebook.com/teamquintadomosteiro/