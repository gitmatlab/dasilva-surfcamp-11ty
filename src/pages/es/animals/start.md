---
tags: animals
partial: start
---

# Animals

**Achtung neu in 2023:**

**Die Happy Horses werden in der Saison 2023 leider nicht mehr bei uns im Camp sein. In unserer Umgebung gibt es aber verschiedene nahegelegen Reiterhöfe bei denen wir gerne Reitstunden oder Reitausflüge für unsere Gäste verabreden.**

**Wir selbst haben geplant auf unserem Gelände eine kleine Farm mit Kleintieren, wie Schafen, Ziegen und Hühnern aufzubauen. Zu dem sind ein bis zwei Ponys geplant.**

Nur 2 km von den schönen weißen Stränden von Areia Branca entfernt lebt unsere kleine Horses und Ponies Herde angeschlossen an das Da Silva Surf Camp. Hier bieten wir Euch seit Sommer 2020 erlebnisreiche Aktivitäten rund um Pferde und Ponies für Kinder und kleine Erwachsene. Neben Ponyreiten und geführten Ausritten bieten wir außerdem noch verschiedene Interaktionen an, die euch und euren Kindern ermöglichen, das liebenswerte Lebewesen Pferd näher kennen und verstehen zu lernen.


Uns ist besonders wichtig, dass Mensch und Pferd dabei gemeinsam Freude finden und Spaß daran haben zu kooperieren. Wir glauben fest daran, dass Horese und Ponies auch Happy Menschen hervorbringen. Darum ist uns auch ein freundlicher Umgang mit unseren Pferden wichtig, die artgerecht im Offenstall gehalten und fortwährend gesunderhaltend trainiert und weiter ausgebildet werden. Auch die Pferde und Ponys wollen sich weiterentwickeln, wollen lernen und gefallen und gelobt werden. Kommt uns besuchen und habt Spaß und Freude auf dem Pferderücken oder vom Boden aus. Ihr könnt auf unserem Reitplatz im Da Silva Surfcamp reiten *oder* Ausritte in die angrenzende Natur *oder* zu den Stränden und Seen *oder* Lagunen der näheren Umgebung genießen. Sprecht uns an. Wir planen auch gerne mit euch individuell, damit ihr euren Urlaub genießen und alle Angebote der Region voll ausnutzen könnt.
