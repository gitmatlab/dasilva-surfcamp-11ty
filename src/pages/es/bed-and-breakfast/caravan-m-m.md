---
tags: bedAndBreakfast
partial: caravan-m-m
---

**Caravan M & M** ist ein gemütlicher Wohnwagen mit Platz für 2 Personen. Die Preise dafür sind genau so wie für unsere Mehrbettzimmer. Für zwei Personen hat das den Vorteil, dass sie allein in dem Wohnwagen sind und den Doppelzimmerzuschlag nicht zahlen müssen. Der Campingwagen hat einen Stromanschluss, die eingebaute Toilette darf nicht benutzt werden. Natürlich dürfen die Gäste Badezimmer und Küche im Haupthaus mitbenutzen, so wie die gesamte Aussenanlage. 

Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
