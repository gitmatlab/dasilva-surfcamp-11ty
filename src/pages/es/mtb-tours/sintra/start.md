---
tags: mtbTourSintra
partial: start
---

# Sintra

Fantastische Flow-Trails und anspruchsvolle Passagen im Weltkulturerbe rund um den Palácio Nacional da Pena. Gesamte Dauer mit Pausen: ca. 5 Stunden. Schwierigkeit: überwiegend S0-S1, mit Passagen bis S2 und einer Strecke auf S3-Niveau 