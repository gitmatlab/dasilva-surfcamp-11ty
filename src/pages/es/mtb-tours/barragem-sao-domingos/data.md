---
tags: mtbTourBarragemSaoDomingos
partial: data
---

- Strecke 34 km
- Reine Fahrzeit 2:31 h
- Höhenmeter 455 m
- <a href="https://strava.app.link/LCL8AVXoNZ" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>
