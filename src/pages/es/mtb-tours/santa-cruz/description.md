---
tags: mtbTourSantaCruz
partial: description
---

Die Tour beginnt früh am Morgen mit dem Transfer im Camp-Bus nach Ribamar. Im Tal des Rio Alcabrichel spenden die Bäume Schatten und die Morgenluft ist angenehm kühl. Nach wenigen hundert Metern verlassen wir den Asphalt und steigen in trockenen Feldwegen auf. Das Regenwasser hat hier Wochen zuvor tiefe Rinnen hinterlassen, die beim Anstieg Kraft kosten.

Nach fünf Kilometern sehen wir, ca. 80 Meter über Meereshöhe, das erste Mal den Atlantik. Schon dieser Blick auf die blaue Weite ist beindruckend. Weiter auf der Höhe fahren wir jetzt parallel zur Küste in Richtung Süden. Nach einigen schnellen Trails führt uns eine raue Schotterpiste schließlich von den Hügeln hinab in ein flaches Tal. Vorbei an wirklich hunderte Meter langen Gewächshäusern geht es nun in Richtung Meer. Der Feldweg hier ist sandig und trocken, umso üppiger aber ist das Grün unter den Glasdächern, wo die Tomaten rot leuchten und jetzt, Anfang September, wohl die Ernte kurz bevorsteht.

Nun beginnt der schönste Teil der Tour: Entlang der Klippen fahren wir auf Santa Cruz zu. Die flowigen Trails mit festem Sandboden sind ein Highlight, besonders vor dieser Kulisse. Kleine Sprünge sind ebenso dabei wie vom Wetter glattgeschliffene Felsbrocken, die sich mit etwas Geschick und Schwung auf den 29ern gut klettern lassen. An einigen Stellen führt der Trail gefühlt nur eine Armlänge vom Rand der Klippen entfernt, volle Konzentration ist gefragt.

In Santa Cruz radeln wir die Promenade entlang beginnen den nächsten kurzen Anstieg zurück auf die Klippen. Wieder auf der Höhe folgen weitere Trails mit festem Sandboden und beindruckenden 
Aussichten auf die Küste. Der Atlantik brandet so kraftvoll an den Felsen, dass am Strand ein leichter Dunst in der Luft liegt. Über eine etwas technischere Passage mit steilen Abschnitten fahren wir an den Strand und halten für eine Kaffeepause mit zwei Bica. Die Stärkung ist nötig, denn anschließend geht es zurück nach Norden, wieder über die sehr schönen Trails bei Santa Cruz. Die Route führt uns auf dem Rückweg noch weiter nach Norden, über Praia de Porto Novo und schließlich ins hügelige Hinterland der Küste.

Auf der letzten Abfahrt zurück zum Startpunkt unserer Tour kühlt schon der Fahrtwind.
