---
permalink: /es/urlaubsinfo/index.html
layout: plain-text.njk
tags: info
locale: es
navigation: Urlaubsinfo

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Urlaubsinfo"

# max 158 characters
metaDescription: "Wichtige wohlfühl-Infos zur Verpflegung, Mücken, Wäschewaschen uvm."
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Urlaubsinfo

<div class="h2">Willkommen im
<br/>Da Silva Surfcamp</div>
<div class="h2">Olá e bem vindo!</div>

Wir freuen uns euch als Gäste bei uns begrüßen zu dürfen. Vor euch liegen absolut entspannte Tage voller Sonnenschein und grandioser Wellen.

Das Camp-Team ist stets für euch da – wir stehen bereit für eure Fragen, geben gerne Anregungen oder helfen aus wo es geht. Wir sind dafür da euren Urlaub so zu mit euch zu gestalten, wie ihr es euch vorstellt!

Auf den folgenden Seiten findet ihr eine kleine Zusammenfassung wissenswerter Eckpunkte, um den Urlaub für alle vor Ort gleich angenehm zu halten.

**Wir wünschen euch eine schöne Zeit bei uns**

### Zufriedenheit

Unser größter Wunsch ist, dass ihr mit allem bei uns zufrieden seid und euren Aufenthalt in schönster Erinnerung behaltet. Sprecht uns einfach an, wenn ihr mit dem ein oder anderen nicht so zufrieden seid, wir werden uns bemühen, das schnellstmöglich zu ändern.

### Frühstück

Täglich erwartet euch am Morgen ein frisches, reichhaltiges Frühstück in der Küche des Haupthauses. Die Frühstückszeiten sind abhängig vom Pick-Up der Surfschule. Allgemein gilt – Frühstück eine Stunde vor Abfahrt. Das gilt für alle. (Sollte der Pick-up sehr früh sein, bleibt das Frühstück natürlich für alle stehen, die noch vor Ort sind). Sind alle durch, wird das Frühstück von uns abgeräumt, spätestens aber um 10 Uhr. Bei der allgemeinen Sauberkeit in der Küche bitten wir euch allerdings um Mithilfe – euer Geschirr und Besteck kommt in die Spülmaschine, benutzte Töpfe und Pfannen werden nach dem Essen selbstständig abgespült.

### Kühlschränke

Während eures Aufenthaltes gibt es täglich ein leckeres Frühstück und an zwei Abenden der Woche (immer Sonntag und Freitag) veranstalten wir gemeinsam ein ausgiebiges BBQ. Wir bitten euch, das Frühstücksbuffet nicht zu nutzen, um euch für den weiteren Tag ein Lunch Paket zu schmieren, so kalkulieren wir nicht und jede*r soll vom Frühstück satt werden ;)

Für eure weitere Verpflegung stehen euch die Gästekühlschränke im Haupthaus zur Verfügung sowie die Fächer im großen Regal. Diese sind mit den Zimmernamen versehen. Seid ihr ohne Auto vor Ort – kein Problem, wir fahren euch gern zum Supermarkt nach Lourinhã, sprecht uns einfach an. Weiterhin findet ihr in Praia da Areia Branca einen kleinen Supermarkt, einen Obstladen und rechts daneben sogar einen kleinen Markt. Solltet ihr mal Essen gehen oder bestellen wollen, sprecht uns einfach an – neben Empfehlungen können wir auch gerne Fahrten mit euch unternehmen.

### Fliegen und Mücken

Leider gibt es bei uns sehr viele Fliegen. Es ist daher absolut wichtig, dass stets alle Türen und Fenster, die nicht mit einem Fliegengitter versehen sind, geschlossen bleiben.

Gleiches gilt für die Mücken. Vor Allem in der Abenddämmerung müssen alle Fenster und Türen absolut geschlossen sein. Leider schaffen es aber immer wieder einige Mücken in die Schlafzimmer zu kommen und unseren Gästen schlaflose Nächte zu bereiten.
Ruhezeiten

Surfen schlaucht – manche von euch wissen das schon, manche werden das schnell merken. Gemütliche Runden sollen nicht unterbrochen werden, aber haltet bitte fest, dass wir ab 23:00 Uhr die Bettruhe einläuten. Bitte nehmt darauf Rücksicht, wenn Leute schon früher schlafen wollen.

### Handtücher

Jeder Gast bekommt bei seiner Anreise ein großes und ein kleines Handtuch. Diese werden donnerstags und sonntags automatisch von uns ausgetauscht. Solltet ihr aus irgendeinem Grund zwischendurch mal ein frisches Handtuch benötigen, sprecht uns bitte darauf an. Bitte nehmt diese Handtücher aber nicht mit an den Strand! Dafür solltet ihr eigene Badetücher haben.

### Bettwäsche

Die Betten sind bei der Anreise frisch bezogen. Wenn ihr länger als eine Woche bei uns seid, könnt ihr gern frische Bettwäsche bekommen. Bitte sprecht uns darauf an.
Schlüssel

Zu jedem Zimmer gibt es einen Schlüssel, der im Schloss steckt, wenn ihr anreist. Bei den Tiny Houses gitb es vorne eine Schlüsselbox. Wenn möglich lasst eure Zimmer donnerstags und sonntags auf, damit wir frische Handtücher hinlegen und einmal durchsaugen können.

### Waschmaschine

Wenn ihr eure Wäsche waschen wollt, könnt ihr gern die Waschmaschine in unserer Waschküche benutzen. Wir berechnen dafür 5 EUR pro Maschine inklusive des Waschmittels. Wenn sich das für eine Person nicht lohnt, könnt Ihr natürlich auch gern zusammenlegen, um eine Waschmaschine voll zu bekommen.

### Wäsche trocknen

Auf dem Gelände findet ihr mehrere Spots, wo ihr eure Strandtücher und Wetsuits zum Trocknen aufhängen könnt. Zum einen findet ihr Wäscheleinen hinter dem Spielplatz und zum anderen an der Hauswand neben der Küche.

### Ordnung und Sauberkeit

Bitte achtet generell darauf, immer alles so zu hinterlassen, wie ihr es vorgefunden habt. Wir bitten euch, benutztes Geschirr, Besteck, Töpfe und Pfannen selbst abzuwaschen, bzw. die Spülmaschine zu benutzen. Gleiches gilt für leere Flaschen, Gläser – diese noch gern am selben Abend wegräumen – die Mitarbeitenden danken es euch ;)

### Zigaretten

Lediglich im Außenbereich ist das Rauchen erlaubt, achtet aber bitte darauf keine Kippenstummel auf den Boden fallen zu lassen – zum einen wegen der Umweltauswirkungen (die schon bei einer einzigen Kippe immens sind!), zum anderen muss keine zweite Person sie wieder aufheben. Außerdem kann schon eine unachtsam weggeworfene Zigarette in den trockenen Sommermonaten ein Feuer entfachen.

### Shuttlebus

Für den Surfkurs werdet ihr jeden Morgen von der RIPAR-Surfschool bei uns abgeholt und nach dem Surfen wieder zurückgebracht. Natürlich könnt ihr danach weiterhin am Strand Zeit verbringen, wenn ihr das wollt.

Je nachdem wie sich das mit unserem Zeitplan vereinbaren lässt, können wir euch auch mal wo hinfahren oder abholen. Bitte sprecht uns deswegen einfach an. Ansonsten kann man mit UBER in Portugal günstig von A nach B kommen :)

### Freizeitangebote

...gibt es nahezu unbegrenzt: Kart fahren, Reiten, Tennis, Wandern, Kiten, Dinopark und vieles mehr. Wir kennen uns hier in der Gegend sehr gut aus und können euch viele Tipps geben. Wir wollen auf keinen Fall, dass ihr euch bei uns langweilt! Sprecht uns daher bitte einfach darauf an und wir organisieren gemeinsam mit euch spannende Ausflüge. Viele Dinge unternehmen wir auch sehr gern mit euch zusammen.

### Abreise mit dem Expressbus

Spätestens einen Tag vor der Abreise solltet ihr ein Busticket am Busbahnhof in Lourinhã kaufen. Es kann durchaus vorkommen, dass der Expressbus ausgebucht ist und man nicht mitgenommen wird und im schlimmsten Fall seinen Flug verpasst. Bitte unbedingt darauf achten, dass das Ticket von Praia da Areia Branca nach Lissabon geht und nicht erst ab Lourinhã. Wer eine Kreditkarte hat, kann das Ticket auch online kaufen. Solltet ihr dabei Unterstützung brauchen, sagt uns einfach Bescheid, wir helfen gern.

### Beachcruiser

Wenn schon Aktiv-Urlaub, dann richtig! In der Gegend ist alles schnell auch mit dem Rad zu erreichen. Wir empfehlen euch also, unsere Beachcruiser zu benutzen, die ihr bei uns jederzeit gegen eine Kaution von 50€ ausleihen könnt. Damit seid ihr völlig unabhängig und könnt jederzeit schnell zum Strand und zurück oder zum Einkaufen nach Lourinhã. Vor allem aber, wenn ihr abends in Areia Branca ausgehen wollt, ist so ein Bike sehr sinnvoll, um wieder heil nach Hause zu kommen.

### Beachcruiserverleih

Die Beachcruiser waren in der Anschaffung ziemlich teuer, daher gilt: wenn ihr euch bei uns einen der Beachcruiser ausleiht, tragt ihr auch die Verantwortung für die Räder, geht also bitte sorgsam damit um und nutzt die von uns dafür bereit gestellten Schlösser. Bei Verlust der Räder kommt ihr für den vollen Preis der Neuanschaffung auf.
Um das zu vermeiden, bitte wir euch die folgenden Regeln streng einzuhalten:
- Die Beachcruiser immer an einem festen Gegenstand, z.B. an einer Straßenlaterne, anschließen.
- Nicht nur an den Reifen, sondern immer auch den Rahmen mit anschließen.
- Immer die beiden Lampen abnehmen, wenn die Beachcruiser irgendwo unbeaufsichtigt stehen.
- Auf keinen Fall die Beachcruiser über Nacht irgendwo stehen lassen, auch wenn sie gut angeschlossen sind.

Viele Infos, solltet ihr dennoch was auf dem Herzen haben sind wir jederzeit für eure Fragen da, sprecht uns einfach oder meldet euch im voraus via Whatsapp oder Signal: +351 / 261 461 515

**Hang Loose**

**Euer Team vom Da Silva - Surfcamp**