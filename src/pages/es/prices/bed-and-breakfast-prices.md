---
tags: prices
partial: bedAndBreakfastPrices
---

- Low Season
  - (October – June)
   - Multiple: 25 €
  - Double: 30 €
  - Suite: 35 €
  - Tiny House: 40 €
  - Single: 55 €
- High Season
  - (July – September)
  - Multiple: 30 €
  - Double: 35 €
  - Suite: 40 €
  - Tiny House: 45 €
  - Single: 65 €
