---
permalink: /es/preise/index.html
layout: prices.njk
tags: prices
locale: es
navigation: Preise

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Preise"

# max 158 characters
metaDescription: "Unterkunft ab 25€ | Surfing Package ab 395€ Woche | Surf & Mountainbike Package ab 505€ Woche | Surf School ab 60€ Tag | Camping ab 10€"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---
