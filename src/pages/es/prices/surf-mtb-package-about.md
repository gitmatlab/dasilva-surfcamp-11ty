---
tags: prices
partial: surf-mtb-package-about
---

## Surf & MTB Package

### ab 475 € pro Woche / Person

Die Kombination von Surfing mit Mountainbiken! Ideal für jeden Surfer, der im Urlaub auch mal mountainbiken möchte, und für jeden Mountainbiker der im Urlaub auch mal surfen möchte. Das gute dran: Man kann die Tage so legen, dass bei guten Wellen gesurft wird und an den anderen Tagen geht’s auf die MTB Trails. 3 Tage Surfing und 2 Tage Mountainbiken oder umgekehrt, geht noch mehr in einer Woche?
