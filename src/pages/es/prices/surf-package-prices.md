---
tags: prices
partial: surfPackagePrices
---

- Low Season
  - (October – June)
  - Multiple: 415 €
  - Double: 450 €
  - Suite: 485 €
  - Tiny House: 520 €
  - Single: 625 €
- High Season
  - (July – September)
  - Multiple: 470 €
  - Double: 505 €
  - Suite: 540 €
  - Tiny House: 575 €
  - Single: 715 €
