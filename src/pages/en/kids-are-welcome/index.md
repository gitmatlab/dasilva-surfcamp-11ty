---
permalink: /en/kids-are-welcome/index.html
layout: kids-are-welcome.njk
tags: kidsAreWelcome
locale: en
navigation: Kids Welcome

title: "Kids are Welcome"

metaImage: "/_assets/teasers/kids-are-welcome.jpg"
metaDescription: "Fun for the whole family | Surfing on the beach with mom and dad or in the sea | Horse riding at the camp | many other children"
---
