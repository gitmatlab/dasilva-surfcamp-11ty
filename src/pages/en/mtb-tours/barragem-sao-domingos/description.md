---
tags: mtbTourBarragemSaoDomingos
partial: description
---

The fifth and last tour leads us from Da Silva Bike camp along the coast to Consolação. After yesterday's tour to Montejunto, we take it a little slower today.

On dirt roads and trails, we follow the coast, with two short descents to the water, in the northern direction to Praia da Consolação. From here we head inland to Atouguia da Baleia. Opposite the impressive church of Nossa Senhora da Conceição we take a break. For refreshment, we have a bica and some pickled tremoços (lupine seeds).

During the following round through the village, we see the second church Igreja de São Leonardo, and notice once more how house dogs serve here both as "alarm system" and as "doorbell" - fortunately only purely acoustically.

Along the reservoir, we head back in a southerly direction, through between the pumpkin fields and over small trails. We could have added the second part of the home trail (from our round on the first day) here, but we decide to be back in camp in time for the BBQ and let the diverse, as well as the challenging week, come to an end on the Fullies.
