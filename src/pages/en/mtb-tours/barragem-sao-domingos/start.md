---
tags: mtbTourBarragemSaoDomingos
partial: start
---

# Barragem de São Domingos

Along the coast to Consoloção to the reservoir of Atouguia da Baleia. A varied house circuit with various technical passages and exciting scenery. Total duration with breaks: about 3.5 hours Difficulty: S0, few passages up to S1.
