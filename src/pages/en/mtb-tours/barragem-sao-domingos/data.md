---
tags: mtbTourBarragemSaoDomingos
partial: data
---

- Distance 34 km
- Moving time 2:31 h
- Elevation 455 m
- <a href="https://strava.app.link/LCL8AVXoNZ" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>
