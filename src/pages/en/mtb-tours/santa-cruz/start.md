---
tags: mtbTourSantaCruz
partial: start
---

# Ribamar – Vimeiro –<br>Santa Rita - Santa Cruz 

From Ribamar through the hills and along the cliffs to Santa Cruz Total duration with breaks: about 5 hours Difficulty: S0, with passages up to S1, few places up to S2.  
