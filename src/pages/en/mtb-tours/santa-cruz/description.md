---
tags: mtbTourSantaCruz
partial: description
---

The tour starts early in the morning with the camp bus transfer to Ribamar. In the valley of the Rio Alcabrichel, the trees provide shade and the morning air is pleasantly cool. After a few hundred meters we leave the asphalt and climb up to dry dirt roads. The rainwater has left deep gullies here weeks before, which cost strength during the ascent.

After 5 km, about 80 meters above sea level, we see the Atlantic Ocean for the first time. Already this view of the blue expanse is impressive. Continuing on the high ground, we now ride parallel to the coast in a southerly direction. After some fast trails, a rough gravel road finally leads us down from the hills into a flat valley. Passing greenhouses that are really hundreds of meters long, we now head towards the sea. The dirt road here is sandy and dry, but all the lusher is the green under the glass roofs, where the tomatoes glow red and now, in early September, the harvest is probably imminent.

Now the most beautiful part of the tour begins: Along the cliffs, we drive towards Santa Cruz. The flowing trails with firm sandy soil are a highlight, especially against this backdrop. There are small jumps as well as boulders ground smooth by the weather, which can be climbed well with a little skill and momentum on the 29ers. In some places, the trail feels like it's only an arm's length away from the edge of the cliffs, so full concentration is required.

In Santa Cruz we cycle along the promenade start the next short climb back up the cliffs. Again on the height follow further trails with firm sandy soil and impressive cost views. The Atlantic crashes so powerfully against the rocks that there is a slight haze in the air on the beach. Over one more technical passage with steep sections, we drive to the beach and stop for a coffee break with two Bica. The refreshment is necessary, because afterward, we head back north, again over the very nice trails near Santa Cruz. The route takes us even further north on the way back, via Praia de Porto Novo and finally into the hilly hinterland of the coast.

On the last descent back to the starting point of our tour, the wind is already cooling.
