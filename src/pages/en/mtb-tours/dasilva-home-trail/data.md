---
tags: mtbTourDasilvaHomeTrail
partial: data
---

- Distance 39 km
- Moving time 3:25 h
- Elevation 624 m
- <a href="https://strava.app.link/PmdAGJILEZ" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>

