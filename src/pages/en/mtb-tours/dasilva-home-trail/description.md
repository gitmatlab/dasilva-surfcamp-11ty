---
tags: mtbTourDasilvaHomeTrail
partial: description
---

We start directly at Da Silva Surf & Bike camp. After two minutes of riding, the first small test of courage awaits a steep slope down to the fields. No big obstacles and plenty of runouts and the biker's heart beat faster immediately. Brakes on and down!

On the field paths, we pass the next village and head east, off into the hills. The ground is sometimes sandy, sometimes stony. Between trees, we drive cross-country and it seems as if Daniel finds the way even blindfolded.

Below a quarry, through which we will make an exciting round later, the trail leads us to Pena Seca. A mural offers Daniel the occasion to tell what the name of the place is all about.

From here we stay at the same altitude for about 10 kilometers. Different trails lead us between stone walls on old paths through bushy backcountry and small woods. The trails are varied: sometimes smoother and brisk to ride, then rocky and more technical, but without big climbs. The little rest in Moledo is as welcome as well as the alcohol-free Sagres. There is also a story to this place, which Daniel knows to tell.

Behind Moledo, it's another 10 kilometers of trails and dirt roads until the next climb gives us a panoramic view of Lourinha and the area. After a quick descent, we reach the town. Towards the coast, a short uphill stretch awaits us, for which we are rewarded with a magnificent view of the bay of Praia Areia Branca and the Atlantic Ocean. On flowing trails, we head towards the beach. The short break directly at the sea creates a great contrast to the winding trails in the hinterland.

The climb back to camp still requires a short effort, but thanks to the scenery the tired legs are quickly forgotten.
