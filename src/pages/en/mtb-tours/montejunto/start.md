---
tags: mtbTourMontejunto
partial: start
---

# Montejunto

The "Serra de Montejunto" is located about 40 km southeast of our camp and is particularly scenic. Lonely single trails lead here through untouched rough landscape. Total duration with breaks: about 5 hours Difficulty: S0-S1, with passages up to S2.
