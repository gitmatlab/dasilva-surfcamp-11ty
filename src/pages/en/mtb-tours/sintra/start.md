---
tags: mtbTourSintra
partial: start
---

# Sintra

Fantastic flow trails and challenging passages in the World Heritage Site around the Palácio Nacional da Pena. Total duration with breaks: about 5 hours. Difficulty: mostly S0-S1, with passages up to S2 and one trail at S3 level.