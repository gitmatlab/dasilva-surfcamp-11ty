---
permalink: /en/surf-bar/index.html
layout: surf-bar.njk
tags: surfBar
locale: en
navigation: Surf Bar

title: "Da Silva Surf Bar"

metaImage: "/_assets/teasers/surf-bar.jpg"
metaDescription: "Drinking and having a good time"
---
