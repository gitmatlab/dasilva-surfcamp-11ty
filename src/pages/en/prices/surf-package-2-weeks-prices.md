---
tags: prices
partial: surfPackage2WeeksPrices
---

- Low Season
  - (October – June)
  - Multiple: 810 €
  - Double: 880 €
  - Suite: 860 €
  - Tiny House: 1.160 €
  - Tiny House Deluxe: 1.300 €
  - Single: 1.230 €
- High Season
  - (July – September)
  - Multiple: 980 €
  - Double: 1.050 €
  - Suite: 1.120 €
  - Tiny House: 1.330 €
  - Tiny House Deluxe: 1.610 €
  - Single: 1.400 €
