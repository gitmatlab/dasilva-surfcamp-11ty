---
tags: prices
partial: mtb-package-prices
---

- Low Season
  - (October – June)
  - Multiple: 475 €
  - Double: 510 €
  - Suite: 545 €
  - Tiny House: 650 €
  - Tiny House Deluxe: 720 €
  - Single: 685 €
- High Season
  - (July – September)
  - Multiple: 545 €
  - Double: 580 €
  - Suite: 615 €
  - Tiny House: 720 €
  - Tiny House Deluxe: 860 €
  - Single: 755 €
