---
tags: prices
partial: surf-mtb-package-prices
---

- Low Season
  - (October – June)
  - Multiple: 475 €
  - Double: 510 €
  - Suite: 545 €
  - Tiny House: 650 €
  - Tiny House Deluxe: 720 €
  - Single: 685 €
- High Season
  - (July – September)
  - Multiple: 560 €
  - Double: 595 €
  - Suite: 630 €
  - Tiny House: 735 €
  - Tiny House Deluxe: 875 €
  - Single: 770 €
