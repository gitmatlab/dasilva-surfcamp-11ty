---
tags: home
partial: feedback
---

### And this is what our guests write:

"We had an awesome time at Da Silva Surfcamp - the casual grounds designed with a lot of charm and creativity offer the ideal space for the whole family to relax: Surf sessions at the numerous spots in the immediate vicinity, extensive beach days, chill-out in the camp, yoga, pony rides, skating, campfires under the starry sky, sociable bar evenings with table-football tournaments, the breakfast with gluten-free & vegan options, delicious BBQ evenings and simply relaxed people - we leave with countless wonderful memories in our luggage. But what shaped the time for us above all and the cherry on the cake was the first-class Team 2022, which made the Da Silva Surfcamp an absolute happy place for us - thanks to everyone for pampering us - we have extended our stay (not just once :-) and will be happy to come back!"
