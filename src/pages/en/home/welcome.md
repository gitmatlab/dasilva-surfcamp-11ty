---
tags: home
partial: welcome
---

<div class="h3" style="margin:0;">The Portuguese Spirit of Surfing</div>
<div class="h4" style="margin:0 0 -1.6rem 0;">in</div>

<h1 style="margin:3.2rem 0 0 0">DA SILVA SURFCAMP</h1>
<h2 style="margin:0 0 1.6rem 0">Portugal</h2>

At Da Silva Surfcamp you are close to nature, the sea and the Portuguese culture and joie de vivre. First class surf spots with little crowd offer perfect conditions for beginners and advanced surfers. Legendary BBQs and spontaneous campfire sessions provide unforgettable moments!


