---
tags: bookingRequest
partial: form

contact:
  label: You Contact Details
firstName:
  label: First Name
lastName:
  label: Last Name
address:
  label: Address
phone:
  label: Telefone
  placeholder: +44 30 123 456 78
email:
  label: E-Mail
arrival:
  label: Arrival
departure:
  label: Departure
travelers:
  label: Additional Travelers 
evenMoreTravelers:
  hint: Please enter other travellers in the comment field below. Please also indicate how many children (up to 12 years) and babies (up to 2 years) are included.

package:
  label: Services
surfPackage1Week:
  label: 1 week Surf Package
mtbPackage1Week:
  label: 1 week MTB Package
SurfMtbPackage1Week:
  label: 1 week Surf & MTB Package
surfPackage2Weeks:
  label: 2 weeks Surf Package
mtbPackage2Weeks:
  label: 2 weeks MTB Package
SurfMtbPackage2Weeks:
  label: 2 weeks Surf & MTB Package
BedAndBreakfastOnly:
  label: only Bed & Breakfast
surfPackageOnly:
  label: only Surf Classes
mtbPackageOnly:
  label: only Mountainbiking

bedAndBreakfast:
  label: Bed and Breakfast
multiBedRoom:
  label: Shared Room
doubleRoom:
  label: Double Room
singleRoom:
  label: Single Room
suite:
  label: Suite
tinyHouse:
  label: Tiny House
surfLodge:
  label: Surf Lodge
camping:
  label: Camping

bookingOptions:
  label: Booking Options
airportTransfer:
  label: We would like to be picked up from the airport.
  hint: Tips for the

comment:
  label: Comment

valueMissing:
  hint: Please specify
patternMismatch:
  email: Please check email

submit:
  label: Submit

---

### Da Silva Surfcamp Portugal
# Booking Form

If you would like to spend your holiday with us and surf and/or mountain bike with us, please fill out the following form:

{% include "booking-form.njk" %}
