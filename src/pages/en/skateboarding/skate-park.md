---
tags: skateboarding
partial: skatePark
---

For the passionate skaters amongst you, we warmly recommend the Skatepark Lourinhã – just 3 km south from Praia da Areia Branca. You can expect sweet ramps, stairs, banks and quite nice pipes! Let´s roll!

<a href="https://bit.ly/2TOFqXt" target="_blank"><img src="/_assets/icons/svgrepo/google-maps.svg" class="surroundings-cards__map-link" alt="google maps link"></a>
