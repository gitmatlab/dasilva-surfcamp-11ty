---
tags: bedAndBreakfast
partial: quarto-azul
subtitle: Girls-4-Bedroom
---

Our 4-bed room on the ground floor can be reached directly from the fireplace room. There are two bunk beds and the guests have their own shower room at their disposal. Right next to the door there is a guest WC in the fireplace room. The single beds can only be booked by women. But of course all other genders are also allowed if you book the entire room.

Prices per person and night including breakfast and 2x BBQ per week:

- July to September: 35 EUR
- October to June: 25 EUR
- Occupancy: 1-4 persons

Click here for [booking]({{ links.en.onlineBooking.path }}).
