---
tags: bedAndBreakfast
partial: quarto-vermelho
subtitle: 7 bed suite
---

Our 7 bed suite is in a separate building just around the corner from the kitchen and diagonally across from our bar. The room has 3 bunk beds and above the bathroom with shower there is a loft bed (gallery) with another single mattress ideal for People who are over 2 meters tall because the foot end is open. The room is ideal for large families or larger groups of friends and offers a small kitchenette with fridge. Outside, next to the front door, you can take a wonderful warm shower outdoors and enjoy the view over the wide landscape.

Prices per person and night including breakfast and 2x BBQ per week:

- July to September: 45 EUR
- October to June: 35 EUR
- From 4 people (adults): 35 EUR or 25 EUR
- Minimum occupancy: 3 persons (adults)
- Children up to 12 years 50% discount
- Babies up to 2 years free of charge

Click here for [booking]({{ links.en.onlineBooking.path }}).
