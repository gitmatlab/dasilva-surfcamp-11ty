---
tags: bedAndBreakfast
partial: tiny-house-jorgina
---


AThe wooden house is ideal for people who would like to be part of the social life of a surf camp, but would like to be able to retreat at any time. The cottage has a very pleasant indoor climate. During the day it does not get too hot and at night the pleasant warmth of the sun stays in the house. If you want to cook something, the communal kitchen of the surf camp is at your disposal. On the way there, you will pass directly by our bar, where you can refresh yourself before or after the exhausting kitchen work.

Prices per person and night including breakfast and 2x BBQ per week:

- July to September: 60 EUR
- October to June: 50 EUR
- Minimum occupancy: 2 persons (adults)
- Children up to 12 years 50% discount
- Babies up to 2 years free of charge

Click here for [booking]({{ links.en.onlineBooking.path }}).
