---
tags: animals
partial: start
---

## Animals

**New in 2023:**

On the grounds of the surf camp we now have a small farm with goats and sheep for you and especially our little guests. The children are welcome to go to the animals and feed them at any time.

Unfortunately, the Happy Horses will no longer live with us in the camp in the 2023 season, but have now moved to larger pastures with their owner Christiane in a neighboring village.

Eliott and Timol are still happy to have horse-loving children visit them and are available to surf camp guests 2 to 3 afternoons a week for two hours each. As always, the two-hour program with a maximum of 6 children first includes cleaning, caring for and pretending the ponies together, and then saddling is also practiced together. Then we do some lead exercises to get to know the ponies from the ground and Christiane explains what is important when communicating with these great animals. After a few tricks from the ground work, the children can then take turns swinging into the saddle and, depending on their ability, are led by Christiane or the other children and playfully learn to sit on the ponies in a well-balanced and relaxed manner or to ride alone. Small walks in the surrounding countryside are also possible here. We end the program with unsaddling and caring for the animals and if there is still a little time, you are welcome to help a little bit or to prepare the evening feeding.

The program is suitable for children from 3 to 12 years, but riding is only possible up to a maximum weight of 50kg. Advanced riding children can maybe also ride with Christiane in the terrain. Saddle strength required in all 3 gaits. Children under the age of 5 must be accompanied by a parent. (Even if they don't have to hold hands then)
