---
tags: mountainBike
partial: tours
---

## Our Tours

Usually, we ride about 40-60 km and get to an altitude of 800-1200 meters. The tours are mainly cross-country, but there is always a bit of enduro and sometimes a bit of downhill. This depends mainly on the wishes and requirements of our participants, which we are happy to take into account. The following tours are just examples. There are countless variations of them, which you can see on [![Strava](/_assets/mtb-tours/strava.jpg)](https://www.strava.com/athletes/17635626)
