---
tags: mountainBike
partial: mtb-package-info
---

<div class="h3">Your Bike Gears:</div>

If you want to ride in clipless cycling shoes, you should bring your own pedals and shoes. We also recommend having your own helmet, bike gloves, and a backpack for snacks (water, fruit, and a few muesli bars). Ideally you have a camel-back (drinking water backpack), cycling pants (padded) and functional sports underwear (at least 2x complete). Our guides always have repair kits, tire levers, pumps, spare tubes, a derailleur hanger, and a first aid kit.

We don't do leisurely rides through the countryside, but sporty demanding MTB tours that are not for beginners. General physical fitness and cycling experience are prerequisites for participation.