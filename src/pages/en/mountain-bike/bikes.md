---
tags: mountainBike
partial: bikes
---

## Our Mountain Bikes

The prices for our bike tours include high-quality full-suspension CUBE mountain bikes (Fully). Available are the models: CUBE AMS, Sting, and Stereo with 29-inch wheels and 120mm travel (front and rear).