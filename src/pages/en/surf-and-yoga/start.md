---
tags: yoga
partial: start
---

## Better surfing and wellbeing with Yoga

Find with us the deceleration, focus, and peace you need to arrive completely at yourself. Our yoga sessions offer you the perfect balance to surfing and strengthen your body for a successful surf session.

Yoga and surfing are made for each other - while you're fully in action surfing, you find inner peace in yoga.

We offer a mix of classical Hatha yoga and modern Vinyasa flow tailored to every level. Whether you're a beginner or an experienced yogi, you'll find the right sessions for you with us.

We want to contribute to promoting your health and bringing your mind, body, and nature into harmony and balance.
