---
tags: surroundings
partial: peniche
mapLink: https://goo.gl/maps/MCEuLzm2VbhXNXXW9
---

An authentic fishing town (12 km north) with an old castle at the harbour and many fish restaurants. The island group "Berlengas" can be reached by ferry in one hour.
