---
tags: surroundings
partial: obidos
mapLink: https://goo.gl/maps/sqdvM1BTgng34ARv5
---

A medieval town (25km north) with huge city walls. Inside no cars are allowed. There is a great medieval festival in August.
