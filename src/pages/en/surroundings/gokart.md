---
tags: surroundings
partial: gokart
webLink: www.dinokart.com.pt
---

### Go Kart

Do you fancy speed? Then race with Dinokart. Accelerate and compete with miniature convertible – you can’t get any closer to the asphalt!
