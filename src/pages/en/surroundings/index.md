---
permalink: /en/surroundings/index.html
layout: surroundings.njk
tags: surroundings
locale: en
navigation: Surroundings

title: "Surroundings and Activities"

metaImage: "/_assets/teasers/surroundings.jpg"
metaDescription: "Surfing, Biken and much more"
---
