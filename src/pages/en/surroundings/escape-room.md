---
tags: surroundings
partial: escape-room
webLink: https://enigmabox.pt
mapLink: https://bit.ly/2VsthoE
---

## Escape Room

Have you been out in the fresh air for too long? Then try the Escape Room and a bit of thinking! At the mystery dinner, not only the palate is spoiled…
