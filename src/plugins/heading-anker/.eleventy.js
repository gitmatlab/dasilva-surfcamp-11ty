const linker = require('./index.js')

module.exports = (eleventyConfig) => {
  console.log('read config heading-anker')
  eleventyConfig.namespace('heading-anker', () => {
    console.log("LINKS content", content)
    eleventyConfig.addTransform('heading-anker', (content, outputPath) => {
      try {
        if (outputPath && outputPath.endsWith('.html')) {
          content = linker(content);
        }
      } catch (error) {
        console.error(error);
      }
      return content;
    });
  });
};