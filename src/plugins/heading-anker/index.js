const parseHTML = require('linkedom').parseHTML

/**
 * Finds unsafe anchor tags and adds safer attributes
 * @param html The HTML string
 * @returns The parsed HTML with safe anchor tags
 */
const linker = (html) => {
  const { document } = parseHTML(html);
  const links = [...document.querySelectorAll('a')]

  // console.log('LINKS', JSON.stringify(html))
  console.log('LINKS', links)

  if (links.length > 0) {
    links.map((link) => {
      if (/^(https?\:\/\/|\/\/)/i.test(link.href)) {
        link.target = '_blank';
        link.setAttribute('rel', 'noreferrer');
      }
      return link;
    });
  } else {
    return html;
  }
  return document.toString();
}

module.exports = linker