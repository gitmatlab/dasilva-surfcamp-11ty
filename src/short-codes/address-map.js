const generateImages = require("./images")

async function renderAddressMap({
  address,
  mapSrc
}) {

  const image = await generateImages({
    src: mapSrc,
    alt: 'Da Silve Surfcamp located on the map',
    myClass: 'address-map__image'
  })

  return `
    <div class="address-map">
      <div class="address-map__container">
        <div class="address-map__left">
          ${address}
        </div>
        <div class="address-map__right">
          <a href="https://g.page/DaSilvaSurfcamp">
            ${image}
          </a>
        </div>
      </div>
    </div>
  `
}

module.exports = renderAddressMap
