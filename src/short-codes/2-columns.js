const renderGallery = require('./gallery')

async function render2Columns({
  headline = "", subHeadline = "", leftGallery = {}, rightText = []
}) {
  const h2 = isEmpty(headline) ? '' : `<h3>${headline}</h3>`
  const h3 = isEmpty(subHeadline) ? '' : `<h3>${subHeadline}</h3>`
  return `
    <div class="two-columns">
     ${h2}
     ${h3}
      <div class="two-columns__container">
        <div class="two-columns__left">
          ${await renderGallery(leftGallery)}
        </div>
        <div class="two-columns__right">
          ${renderText(rightText)}
        </div>
      </div>
    </div>
  `
}

function renderText(rightColumn) {
  if (!rightColumn || !rightColumn.length) return ""
  return rightColumn.map(t => t)
}

function isEmpty(string) {
  return string.match(/^\s*$/, '') ? true : false
}

module.exports = render2Columns