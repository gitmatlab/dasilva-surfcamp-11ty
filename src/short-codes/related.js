const generateImages = require("./images")
const translation = require("../data/translation")

async function renderRelated({
  links,
  pages
}) {
  return `<div class="related">
    <div class="related__grid">
      ${(await getPagesGrid(pages, links)).join(" ")}
    </div>
  </div>`
}

async function getPagesGrid(pages, linksData) {

  const grid = pages.map(async (page) => {
    
    
    if (!linksData[page.link]?.image) {
      console.error("check pages with this\nlink:", page.link)
      return
    }

    const image = await generateImages({
      src: linksData[page.link].image,
      alt: linksData[page.link]?.title,
      myClass: 'related__image'
    })

    return `<div class="related__item">
      <a href="${linksData[page.link].path}">
        ${image}
        ${getPageInfo(page, linksData)}
      </a>
    </div>`
  })
  
  return await Promise.all(grid)
}

function getPageInfo(page, linksData) {
  if (page.info) {
    return `<div class=" related__text-box related__info">
    <div class="related__later">${translation[page[translation].info]}</div>
  </div>`
  } else {
    return `<div class="related__text-box">
    <div class="related__title">${linksData[page.link].title}</div>
  </div>`
  }
}

module.exports = renderRelated
