// Last update: 2025-01-19
window.dasilva = window.dasilva || []
window.dasilva.googleTagId = 'AW-16815783416';
// GoogleTag + Conversion ID in /booking-request/response
window.dasilva.googleConversionId = 'AW-16815783416/QcVwCP-6sfYZEPj7sdI-';

const gtagScript = document.createElement('script');
gtagScript.src = `https://www.googletagmanager.com/gtag/js?id=${window.dasilva.googleTagId}`;
gtagScript.async = true;
document.head.appendChild(gtagScript);

window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', window.dasilva.googleTagId);