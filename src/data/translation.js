module.exports = {
  de: {
    aboutCookies: "Wir verwenden Cookies, um zu erfahren, wie Ihr uns gefunden habt. Wir hoffen, das ist OK für Euch. Mehr dazu hier: <a href=\"/de/cookies/\">Cookie-Infos</a>.",
    acceptCookies: "Cookies annehmen",
    rejectCookies: "Marketing-Cookie ablehnen",
    menu:{
      action: "Action",
      booking: "Booking",
      camp: "Camp",
    },
  },
  en: {
    aboutCookies: "Cookies help us with marketing. You can refuse them. For more details checkout the: <a href=\"/en/cookies/\">Cookie Info</a>.",
    acceptCookies: "Accept all Cookies",
    rejectCookies: "Reject Marketing Cookies",
    menu:{
      action: "Action",
      booking: "Booking",
      camp: "Camp",
    },
  },
  fr: {
    aboutCookies: "Cookies help us with marketing. You can refuse them. For more details checkout the: <a href=\"/en/cookies/\">Cookie Info</a>.",
    acceptCookies: "Accept all Cookies",
    rejectCookies: "Reject Marketing Cookies",
    menu:{
      action: "Action",
      booking: "Booking",
      camp: "Camp",
    },
  },
  es: {
    aboutCookies: "Cookies help us with marketing. You can refuse them. For more details checkout the: <a href=\"/en/cookies/\">Cookie Info</a>.",
    acceptCookies: "Accept all Cookies",
    rejectCookies: "Reject Marketing Cookies",
    menu:{
      action: "Action",
      booking: "Booking",
      camp: "Camp",
    },
  },  
}


